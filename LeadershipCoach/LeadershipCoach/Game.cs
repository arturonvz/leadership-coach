﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace NurseSup
{
    public class Competencia
    {
        // Propiedades de la clase que representan las columnas de la tabla
        public long Id { get; set; }
        public string CodigoCompetencia { get; set; }
        public string NombreCompetencia { get; set; }

        // Constructor sin argumentos
        public Competencia()
        {
        }

        // Constructor con un DataRow como parámetro
        public Competencia(DataRow row)
        {
            // Llenar las propiedades de la clase desde el DataRow
            Id = Convert.ToInt64(row["id"]);
            CodigoCompetencia = row["CódigoCompetencia"].ToString();
            NombreCompetencia = row["NombreCompetencia"].ToString();
        }

        // Método para llenar la instancia de la clase desde un DataRow
        public void LlenarDesdeDataRow(DataRow row)
        {
            // Llenar las propiedades de la clase desde el DataRow
            Id = Convert.ToInt64(row["id"]);
            CodigoCompetencia = row["CódigoCompetencia"].ToString();
            NombreCompetencia = row["NombreCompetencia"].ToString();
        }
    }


    public class Habilidad
    {
        // Propiedades de la clase que representan las columnas de la tabla
        public long Id { get; set; }
        public string CodigoHabilidad { get; set; }
        public string NombreHabilidad { get; set; }
        public string CodigoCompetencia { get; set; }

        // Constructor sin argumentos
        public Habilidad()
        {
        }

        // Constructor con un DataRow como parámetro
        public Habilidad(DataRow row)
        {
            // Llenar las propiedades de la clase desde el DataRow
            Id = Convert.ToInt64(row["id"]);
            CodigoHabilidad = row["CódigoHabilidad"].ToString();
            NombreHabilidad = row["NombreHabilidad"].ToString();
            CodigoCompetencia = row["CódigoCompetencia"].ToString();
        }

        // Método para llenar la instancia de la clase desde un DataRow
        public void LlenarDesdeDataRow(DataRow row)
        {
            // Llenar las propiedades de la clase desde el DataRow
            Id = Convert.ToInt64(row["id"]);
            CodigoHabilidad = row["CódigoHabilidad"].ToString();
            NombreHabilidad = row["NombreHabilidad"].ToString();
            CodigoCompetencia = row["CódigoCompetencia"].ToString();
        }
    }


    public class Nivel
    {
        // Propiedades de la clase que representan las columnas de la tabla
        public long Id { get; set; }
        public string Nombre { get; set; }
        public int Fase { get; set; }
        public bool Desbloqueado { get; set; }
        public int Dificultad { get; set; }
        public long IdHabilidad { get; set; }
        public string DesbloquearWhere { get; set; }
        public long DesbloquearId { get; set; }
        public int Desbloqueos { get; set; }
        public int DesbloqueosPerfecto { get; set; }

        // Constructor sin argumentos
        public Nivel()
        {
        }

        // Constructor con un DataRow como parámetro
        public Nivel(DataRow row)
        {
            // Llenar las propiedades de la clase desde el DataRow
            Id = Convert.ToInt64(row["id"]);
            Nombre = row["Nombre"].ToString();
            Fase = Convert.ToInt32(row["Fase"]);
            Desbloqueado = Convert.ToBoolean(row["Desbloqueado"]);
            Dificultad = Convert.ToInt32(row["Dificultad"]);
            IdHabilidad = Convert.ToInt64(row["idHabilidad"]);
            DesbloquearWhere = row["DesbloquearWhere"].ToString();
            DesbloquearId = Convert.ToInt64(row["DesbloquearId"]);
            Desbloqueos = Convert.ToInt32(row["Desbloqueos"]);
            DesbloqueosPerfecto = Convert.ToInt32(row["DesbloqueosPerfecto"]);
        }

        // Método para llenar la instancia de la clase desde un DataRow
        public void LlenarDesdeDataRow(DataRow row)
        {
            // Llenar las propiedades de la clase desde el DataRow
            Id = Convert.ToInt64(row["id"]);
            Nombre = row["Nombre"].ToString();
            Fase = Convert.ToInt32(row["Fase"]);
            Desbloqueado = Convert.ToBoolean(row["Desbloqueado"]);
            Dificultad = Convert.ToInt32(row["Dificultad"]);
            IdHabilidad = Convert.ToInt64(row["idHabilidad"]);
            DesbloquearWhere = row["DesbloquearWhere"].ToString();
            DesbloquearId = Convert.ToInt64(row["DesbloquearId"]);
            Desbloqueos = Convert.ToInt32(row["Desbloqueos"]);
            DesbloqueosPerfecto = Convert.ToInt32(row["DesbloqueosPerfecto"]);
        }
    }

    public class Escena
    {
        public long Id { get; set; }
        public string Nombre { get; set; }
        public long? IdNivel { get; set; }
        public int? Correlativo { get; set; }
        public string TextoSuperior { get; set; }
        public string TextoInferior { get; set; }
        public string TextoMedio { get; set; }
        public string ImagenFondoURL { get; set; }
        public bool? RelativeURL { get; set; }

        public Escena()
        {
            // Constructor sin argumentos
        }

        public Escena(DataRow row)
        {
            Id = row.Field<long>("id");
            Nombre = row.Field<string>("Nombre");
            IdNivel = row.Field<long?>("idNivel");
            Correlativo = row.Field<int?>("Correlativo");
            TextoSuperior = row.Field<string>("TextoSuperior");
            TextoInferior = row.Field<string>("TextoInferior");
            TextoMedio = row.Field<string>("TextoMedio");
            ImagenFondoURL = row.Field<string>("ImagenFondoURL");
            RelativeURL = row.Field<bool?>("RelativeURL");
        }

        public void UpdateFromDataRow(DataRow row)
        {
            Id = row.Field<long>("id");
            Nombre = row.Field<string>("Nombre");
            IdNivel = row.Field<long?>("idNivel");
            Correlativo = row.Field<int?>("Correlativo");
            TextoSuperior = row.Field<string>("TextoSuperior");
            TextoInferior = row.Field<string>("TextoInferior");
            TextoMedio = row.Field<string>("TextoMedio");
            ImagenFondoURL = row.Field<string>("ImagenFondoURL");
            RelativeURL = row.Field<bool?>("RelativeURL");
        }
    }

    public class EscenaFinal
    {
        public long Id { get; set; }
        public string Nombre { get; set; }
        public long? IdNivel { get; set; }
        public int? Correlativo { get; set; }
        public bool? FinalBueno { get; set; }
        public bool? FinalMalo { get; set; }
        public string TextoSuperior { get; set; }
        public string TextoInferior { get; set; }
        public string TextoMedio { get; set; }
        public string ImagenFondoURL { get; set; }
        public bool? RelativeURL { get; set; }

        public EscenaFinal()
        {
            // Constructor sin argumentos
        }

        public EscenaFinal(DataRow row)
        {
            Id = row.Field<long>("id");
            Nombre = row.Field<string>("Nombre");
            IdNivel = row.Field<long?>("idNivel");
            Correlativo = row.Field<int?>("Correlativo");
            FinalBueno = row.Field<bool?>("FinalBueno");
            FinalMalo = row.Field<bool?>("FinalMalo");
            TextoSuperior = row.Field<string>("TextoSuperior");
            TextoInferior = row.Field<string>("TextoInferior");
            TextoMedio = row.Field<string>("TextoMedio");
            ImagenFondoURL = row.Field<string>("ImagenFondoURL");
            RelativeURL = row.Field<bool?>("RelativeURL");
        }

        public void UpdateFromDataRow(DataRow row)
        {
            Id = row.Field<long>("id");
            Nombre = row.Field<string>("Nombre");
            IdNivel = row.Field<long?>("idNivel");
            Correlativo = row.Field<int?>("Correlativo");
            FinalBueno = row.Field<bool?>("FinalBueno");
            FinalMalo = row.Field<bool?>("FinalMalo");
            TextoSuperior = row.Field<string>("TextoSuperior");
            TextoInferior = row.Field<string>("TextoInferior");
            TextoMedio = row.Field<string>("TextoMedio");
            ImagenFondoURL = row.Field<string>("ImagenFondoURL");
            RelativeURL = row.Field<bool?>("RelativeURL");
        }
    }

    public class PreguntaItem
    {
        public long Id { get; set; }
        public long? IdHabilidad { get; set; }
        public int? Dificultad { get; set; }
        public string Pregunta { get; set; }
        public string Respuesta { get; set; }
        public string RespuestaFalsa1 { get; set; }
        public string RespuestaFalsa2 { get; set; }
        public string RespuestaFalsa3 { get; set; }

        public PreguntaItem()
        {
            // Constructor sin argumentos
        }

        public PreguntaItem(DataRow row)
        {
            Id = row.Field<long>("id");
            IdHabilidad = row.Field<long?>("idHabilidad");
            Dificultad = row.Field<int?>("Dificultad");
            Pregunta = row.Field<string>("Pregunta");
            Respuesta = row.Field<string>("Respuesta");
            RespuestaFalsa1 = row.Field<string>("RespuestaFalsa1");
            RespuestaFalsa2 = row.Field<string>("RespuestaFalsa2");
            RespuestaFalsa3 = row.Field<string>("RespuestaFalsa3");
        }

        public void UpdateFromDataRow(DataRow row)
        {
            Id = row.Field<long>("id");
            IdHabilidad = row.Field<long?>("idHabilidad");
            Dificultad = row.Field<int?>("Dificultad");
            Pregunta = row.Field<string>("Pregunta");
            Respuesta = row.Field<string>("Respuesta");
            RespuestaFalsa1 = row.Field<string>("RespuestaFalsa1");
            RespuestaFalsa2 = row.Field<string>("RespuestaFalsa2");
            RespuestaFalsa3 = row.Field<string>("RespuestaFalsa3");
        }
    }
}
