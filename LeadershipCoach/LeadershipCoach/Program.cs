﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using ingeniaCore;
using NurseSup.Properties;

namespace NurseSup
{
    public static class UserLogin
    {
        public static User UserData;
        public static bool LoggedIn { get; set; }
    }
    internal static class Program
    {
        [STAThread]
        static void Main()
        {
            UserLogin.UserData = new User();
            UserLogin.LoggedIn = false;

            // Inicializar las nuevas propiedades booleanas

            if (Settings.Default.ConnectionString == "default")
            {
                MessageBox.Show("Archivo de configuración sin cadena de conexión. Modifícalo antes de continuar.", "Error de Configuración", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                Server.SQLDatabase.SQLServer = Settings.Default.SQLServer;
                Server.SQLDatabase.MySQL = Settings.Default.MySQL;
                Server.SQLDatabase.PostgreSQL = Settings.Default.PostgreSQL;
                Server.SQLDatabase.SetPropertiesFromConnectionString(Settings.Default.ConnectionString);

                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new AppForm());
            }
        }
    }
}
