﻿using ingeniaCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NurseSup
{
    public class Login
    {

        //validate string 
        private static bool StringValidator(string input)
        {
            // Patrón para correos electrónicos.
            // Para investigar más patrones, ver regreslib.com
            string pattern = @"^[a-zA-Z]";
            if (Regex.IsMatch(input, pattern))
            {
                return true;
            }
            else
            {
                return true;
            }
        }
        //method to check if eligible to be logged in 
        public static int login(string user, string pass)
        {
            // check user name empty 
            if (string.IsNullOrEmpty(user))
            {
                return -2;
            }
            // check user name is valid type 
            else if (!StringValidator(user))
            {
                return -4;
            }
            // check user name is correct 
            else
            {
                // check password is empty 
                if (string.IsNullOrEmpty(pass))
                {
                    return -3;
                }
                // check user of password is correct 
                else
                {
                    DataTable resultLoginTable = CheckLogin(user, pass);
                    if (resultLoginTable == null)
                    {
                        return -1;
                    }
                    else
                    {
                        if (resultLoginTable.Rows.Count > 0)
                        {
                            DataRow row = resultLoginTable.Rows[0];

                            // correct
                            UserLogin.UserData = new User(row);


                            UserLogin.LoggedIn = true;
                            return 1;
                        }
                        else
                        {
                            return -1;
                        }
                    }
                }
            }
        }

        public static DataTable CheckLogin(string UserName, string Password)
        {
            SQLResponse result;
            result = Server.SQLDatabase.ObtenerTabla("*", "UserName = '" + UserName +
                "' and Password = '" + Password + "'", "ViewUsers", "UserName");
            return result.DataTable;
        }

        public static DataTable CheckLoginSHA256(string UserName, string Password)
        {
            SQLResponse result;
            result = Server.SQLDatabase.ObtenerConteo("*", "UserName = '" + UserName +
                "' and PasswordSHA256 = hashbytes('SHA2_256','" + Password + "')", "ViewUsuarios");
            return result.DataTable;
        }
    }
    internal class ControlsProcedures
    {
        public static void CentrarX(Control control)
        {
            control.Left = (control.Parent.Width - control.Width) / 2;
        }
        public static void CentrarY(Control control)
        {
            control.Top = (control.Parent.Height - control.Height) / 2;
        }
        public static void Centrar(Control control)
        {
            control.Left = (control.Parent.Width - control.Width) / 2;
            control.Top = (control.Parent.Height - control.Height) / 2;
        }
        public static void FillX(Control control)
        {
            control.Left = 0;
            control.Width = control.Parent.Width;
        }
        public static void FillY(Control control)
        {
            control.Top = 0;
            control.Height = control.Parent.Height;
        }
    }
}
