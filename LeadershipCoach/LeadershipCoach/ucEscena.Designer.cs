﻿namespace NurseSup
{
    partial class ucEscena
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelBackground = new System.Windows.Forms.Panel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.panelSuperior = new System.Windows.Forms.Panel();
            this.lblSuperior = new System.Windows.Forms.Label();
            this.panelMedio = new System.Windows.Forms.Panel();
            this.lblMedio = new System.Windows.Forms.Label();
            this.panelInferior = new System.Windows.Forms.Panel();
            this.lblInferior = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.panelBackground.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.panelSuperior.SuspendLayout();
            this.panelMedio.SuspendLayout();
            this.panelInferior.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelBackground
            // 
            this.panelBackground.BackColor = System.Drawing.Color.White;
            this.panelBackground.BackgroundImage = global::NurseSup.Properties.Resources._6867;
            this.panelBackground.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.panelBackground.Controls.Add(this.tableLayoutPanel1);
            this.panelBackground.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelBackground.Location = new System.Drawing.Point(0, 0);
            this.panelBackground.Name = "panelBackground";
            this.panelBackground.Size = new System.Drawing.Size(946, 510);
            this.panelBackground.TabIndex = 0;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel1.ColumnCount = 7;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Controls.Add(this.panelSuperior, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.panelMedio, 2, 3);
            this.tableLayoutPanel1.Controls.Add(this.panelInferior, 2, 5);
            this.tableLayoutPanel1.Controls.Add(this.button1, 5, 2);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 7;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(946, 510);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // panelSuperior
            // 
            this.panelSuperior.BackgroundImage = global::NurseSup.Properties.Resources.OpacityWhite;
            this.tableLayoutPanel1.SetColumnSpan(this.panelSuperior, 3);
            this.panelSuperior.Controls.Add(this.lblSuperior);
            this.panelSuperior.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelSuperior.Location = new System.Drawing.Point(73, 23);
            this.panelSuperior.Name = "panelSuperior";
            this.panelSuperior.Size = new System.Drawing.Size(800, 94);
            this.panelSuperior.TabIndex = 1;
            // 
            // lblSuperior
            // 
            this.lblSuperior.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblSuperior.Font = new System.Drawing.Font("Supercell-Magic", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSuperior.Location = new System.Drawing.Point(0, 0);
            this.lblSuperior.Name = "lblSuperior";
            this.lblSuperior.Size = new System.Drawing.Size(800, 94);
            this.lblSuperior.TabIndex = 0;
            this.lblSuperior.Text = "Texto Superior";
            this.lblSuperior.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panelMedio
            // 
            this.panelMedio.BackgroundImage = global::NurseSup.Properties.Resources.OpacityWhite;
            this.tableLayoutPanel1.SetColumnSpan(this.panelMedio, 3);
            this.panelMedio.Controls.Add(this.lblMedio);
            this.panelMedio.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelMedio.Location = new System.Drawing.Point(73, 208);
            this.panelMedio.Name = "panelMedio";
            this.panelMedio.Size = new System.Drawing.Size(800, 94);
            this.panelMedio.TabIndex = 1;
            this.panelMedio.Visible = false;
            // 
            // lblMedio
            // 
            this.lblMedio.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblMedio.Font = new System.Drawing.Font("Supercell-Magic", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMedio.Location = new System.Drawing.Point(0, 0);
            this.lblMedio.Name = "lblMedio";
            this.lblMedio.Size = new System.Drawing.Size(800, 94);
            this.lblMedio.TabIndex = 0;
            this.lblMedio.Text = "Texto Medio";
            this.lblMedio.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panelInferior
            // 
            this.panelInferior.BackgroundImage = global::NurseSup.Properties.Resources.OpacityWhite;
            this.tableLayoutPanel1.SetColumnSpan(this.panelInferior, 3);
            this.panelInferior.Controls.Add(this.lblInferior);
            this.panelInferior.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelInferior.Location = new System.Drawing.Point(73, 393);
            this.panelInferior.Name = "panelInferior";
            this.panelInferior.Size = new System.Drawing.Size(800, 94);
            this.panelInferior.TabIndex = 1;
            // 
            // lblInferior
            // 
            this.lblInferior.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblInferior.Font = new System.Drawing.Font("Supercell-Magic", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInferior.Location = new System.Drawing.Point(0, 0);
            this.lblInferior.Name = "lblInferior";
            this.lblInferior.Size = new System.Drawing.Size(800, 94);
            this.lblInferior.TabIndex = 0;
            this.lblInferior.Text = "Texto Inferior asdfasdfa asdfasdf asdfasdf asdfasdf";
            this.lblInferior.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Transparent;
            this.button1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button1.FlatAppearance.BorderSize = 4;
            this.button1.FlatAppearance.MouseDownBackColor = System.Drawing.Color.CornflowerBlue;
            this.button1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.RoyalBlue;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Supercell-Magic", 24F);
            this.button1.Location = new System.Drawing.Point(879, 123);
            this.button1.Name = "button1";
            this.tableLayoutPanel1.SetRowSpan(this.button1, 3);
            this.button1.Size = new System.Drawing.Size(44, 264);
            this.button1.TabIndex = 2;
            this.button1.Text = ">";
            this.button1.UseVisualStyleBackColor = false;
            // 
            // ucEscena
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DimGray;
            this.Controls.Add(this.panelBackground);
            this.Name = "ucEscena";
            this.Size = new System.Drawing.Size(946, 510);
            this.panelBackground.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.panelSuperior.ResumeLayout(false);
            this.panelMedio.ResumeLayout(false);
            this.panelInferior.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelBackground;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label lblSuperior;
        private System.Windows.Forms.Panel panelSuperior;
        private System.Windows.Forms.Panel panelMedio;
        private System.Windows.Forms.Label lblMedio;
        private System.Windows.Forms.Panel panelInferior;
        private System.Windows.Forms.Label lblInferior;
        private System.Windows.Forms.Button button1;
    }
}
