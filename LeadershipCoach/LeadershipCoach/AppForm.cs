﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Security.Cryptography;
using ingeniaCore;
using NurseSup;

namespace NurseSup
{
    public partial class AppForm : Form
    {

        UserControl uclogin;
        UserControl ucpesaje;
        ucPregunta ucRandom;

        bool LoginStatus = true;
        string strSerialRead = "";
        public AppForm()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

            //txtPuntosTitulo.Text = string.Concat(Enumerable.Repeat("░", Convert.ToInt32(txtPuntosTitulo.Width / 8.5)));

            tableLayoutPanelMenuHide.Visible = false;
            tableLayoutPanel1.ColumnStyles[0].Width = 0;
            tableLayoutPanelMenuShow.Visible = true;
            tableLayoutPanel1.ColumnStyles[1].Width = 165;
            //WindowState = FormWindowState.Maximized;
        }

        private void lblHideMenu_Click(object sender, EventArgs e)
        {
            tableLayoutPanelMenuHide.Visible = true;
            tableLayoutPanel1.ColumnStyles[0].Width = 45;
            tableLayoutPanelMenuShow.Visible = false;
            tableLayoutPanel1.ColumnStyles[1].Width = 0;
        }

        private void lblShowMenu_Click(object sender, EventArgs e)
        {
            {
                tableLayoutPanelMenuHide.Visible = false;
                tableLayoutPanel1.ColumnStyles[0].Width = 0;
                tableLayoutPanelMenuShow.Visible = true;
                tableLayoutPanel1.ColumnStyles[1].Width = 165;
            }
        }

        private void btnHome_Click(object sender, EventArgs e)
        {
            ScreenNavigate(panelInicio);
            if (ucpesaje != null)
                ucpesaje.Dispose();
            ucpesaje = null;
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            if (uclogin == null)
                uclogin = new ucLogin();

            uclogin.Dock = DockStyle.Fill;
            PanelMain.Controls.Add(uclogin);
            uclogin.Hide();

            ScreenNavigate(uclogin);
        }

        private void ScreenNavigate(Control usercontrol)
        {
            foreach (Control ctrl in PanelMain.Controls)
            {
                ctrl.Hide();
            }
            usercontrol.Show();
        }

        private void btnGame_Click(object sender, EventArgs e)
        {
            //if (ucpesaje == null)
            //    ucpesaje = new ucPesajeBobinas();
            //if (ucpesaje.IsDisposed)
            //{
            //    ucpesaje = new ucPesajeBobinas();

            //    ucpesaje.Dock = DockStyle.Fill;
            //    panel1.Controls.Add(ucpesaje);
            //    ucpesaje.Hide();

            //    ScreenNavigate(ucpesaje);
            //}
            //else
            //{
            //    ucpesaje.Dock = DockStyle.Fill;
            //    panel1.Controls.Add(ucpesaje);
            //    ucpesaje.Hide();

            //    ScreenNavigate(ucpesaje);
            //}
        }

        private void btnHistorial_Click(object sender, EventArgs e)
        {

        }

        private void btnRandom_Click(object sender, EventArgs e)
        {
            // Obtener datos de la tabla "Preguntas" con condición "id > 1" y ordenados por "id"
            SQLResponse sQLResponse = Server.SQLDatabase.ObtenerTabla("*", "id > 1", "Preguntas", "id");

            if (sQLResponse.Count >= 1)
            {
                // Obtener un número aleatorio entre 0 y el número de filas - 1
                Random random = new Random();
                int randomIndex = random.Next(0, sQLResponse.DataTable.Rows.Count);

                // Seleccionar la fila aleatoria
                DataRow drRandom = sQLResponse.DataTable.Rows[randomIndex];

                // Ahora puedes trabajar con la fila aleatoria (drRandom)
                PreguntaItem preguntaItem = new PreguntaItem(drRandom);

                try
                {
                    PanelMain.Controls.Remove(ucRandom);
                }
                catch (Exception)
                {

                }
                ucRandom = new ucPregunta(preguntaItem);

                ucRandom.Dock = DockStyle.Fill;
                ucRandom.Visible = false;
                PanelMain.Controls.Add(ucRandom);
                ScreenNavigate(ucRandom);
            }
        }

        private void btnAlarmas_Click(object sender, EventArgs e)
        {

        }

        private void tmrLogin_Tick(object sender, EventArgs e)
        {
            if (LoginStatus != UserLogin.LoggedIn)
            {
                LoginStatus = UserLogin.LoggedIn;
                if (LoginStatus == true)
                {
                    btnLogin.Text = "          Cerrar sesión:\n          " + UserLogin.UserData.FirstName;
                    btnLogin.Image = NurseSup.Properties.Resources.logged_in_24px;
                    btnLoginS.Image = NurseSup.Properties.Resources.logged_in_24px;

                    try
                    {
                        SQLResponse sqlR = Server.SQLDatabase.ObtenerTabla("*", "id>=1", "Groups", "Id");
                        DataTable Permisos = sqlR.DataTable;
                        
                    }
                    catch (Exception ex)
                    {
                        //functionCallStatusLabel.Text = ex.Message;
                    }
                }
                else
                {
                    btnLogin.Text = "          Iniciar sesión";
                    btnLogin.Image = NurseSup.Properties.Resources.locked_24px;
                    btnLoginS.Image = NurseSup.Properties.Resources.locked_24px;
                }
            }
        }

        private void AppForm_FormClosing(object sender, FormClosingEventArgs e)
        {

        }

        private void btnHabilidad1_Click(object sender, EventArgs e)
        {
            // Obtener datos de la tabla "Preguntas" con condición "id > 1" y ordenados por "id"
            SQLResponse sQLResponse = Server.SQLDatabase.ObtenerTabla("*", "idHabilidad = 1", "Preguntas", "id");

            if (sQLResponse.Count >= 1)
            {
                // Obtener un número aleatorio entre 0 y el número de filas - 1
                Random random = new Random();
                int randomIndex = random.Next(0, sQLResponse.DataTable.Rows.Count);

                // Seleccionar la fila aleatoria
                DataRow drRandom = sQLResponse.DataTable.Rows[randomIndex];

                // Ahora puedes trabajar con la fila aleatoria (drRandom)
                PreguntaItem preguntaItem = new PreguntaItem(drRandom);

                try
                {
                    PanelMain.Controls.Remove(ucRandom);
                }
                catch (Exception)
                {

                }
                ucRandom = new ucPregunta(preguntaItem, "idHabilidad = 1");

                ucRandom.Dock = DockStyle.Fill;
                ucRandom.Visible = false;
                PanelMain.Controls.Add(ucRandom);
                ScreenNavigate(ucRandom);
            }
        }

        private void btnHabilidad2_Click(object sender, EventArgs e)
        {
            // Obtener datos de la tabla "Preguntas" con condición "id > 1" y ordenados por "id"
            SQLResponse sQLResponse = Server.SQLDatabase.ObtenerTabla("*", "idHabilidad = 2", "Preguntas", "id");

            if (sQLResponse.Count >= 1)
            {
                // Obtener un número aleatorio entre 0 y el número de filas - 1
                Random random = new Random();
                int randomIndex = random.Next(0, sQLResponse.DataTable.Rows.Count);

                // Seleccionar la fila aleatoria
                DataRow drRandom = sQLResponse.DataTable.Rows[randomIndex];

                // Ahora puedes trabajar con la fila aleatoria (drRandom)
                PreguntaItem preguntaItem = new PreguntaItem(drRandom);

                try
                {
                    PanelMain.Controls.Remove(ucRandom);
                }
                catch (Exception)
                {

                }
                ucRandom = new ucPregunta(preguntaItem, "idHabilidad = 2");

                ucRandom.Dock = DockStyle.Fill;
                ucRandom.Visible = false;
                PanelMain.Controls.Add(ucRandom);
                ScreenNavigate(ucRandom);
            }
        }
    }
}
