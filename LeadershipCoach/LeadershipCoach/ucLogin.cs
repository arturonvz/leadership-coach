﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ingeniaCore;
using System.Drawing.Drawing2D;

namespace NurseSup
{
    public partial class ucLogin : UserControl
    {
        SQLResponse sqlRUsers = null;
        DataTable dtUsers = null;
        public ucLogin()
        {
            InitializeComponent();
        }

        private void ucLogin_Load(object sender, EventArgs e)
        {
            try
            {
                SQLResponse sqlR = Server.SQLDatabase.ObtenerTabla("UserName", "Id>0", "Users", "UserName");
                if (sqlR.DataTable != null)
                {
                    dtUsers = sqlR.DataTable;
                    cmbUserName.DataSource = sqlR.DataTable;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("No se pudo consultar lista de usuarios de la base de datos.");
            }
            cmbUserName.DrawMode = DrawMode.OwnerDrawFixed; // or DrawMode.OwnerDrawVariable;
            ucLogin_Resize(sender, e);
            if (UserLogin.LoggedIn == false)
            {
                cmbUserName.Enabled = true;
                txtPassword.Enabled = true;
                cmbUserName.Text = "";
                txtPassword.Text = "";
                btnLogin.Text = "INICIAR SESIÓN";
            }
            else
            {
                cmbUserName.Enabled = false;
                txtPassword.Enabled = false;
                cmbUserName.Text = UserLogin.UserData.UserName;
                txtPassword.Text = UserLogin.UserData.Password;
                btnLogin.Text = "CERRAR SESIÓN";
            }
        }

        private void ucLogin_Resize(object sender, EventArgs e)
        {
            ControlsProcedures.CentrarX(panelLogin);

            pbxWrongUser.Left = cmbUserName.Left + cmbUserName.Parent.Left - pbxWrongUser.Width;
            pbxWrongUser.Top = cmbUserName.Top + cmbUserName.Parent.Top + cmbUserName.Height / 2 - pbxWrongUser.Height / 2;
            pbxWrongPassword.Left = txtPassword.Left + txtPassword.Parent.Left - pbxWrongPassword.Width;
            pbxWrongPassword.Top = txtPassword.Top + txtPassword.Parent.Top + txtPassword.Height / 2 - pbxWrongPassword.Height / 2;
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            pbxUserLogin.Image = NurseSup.Properties.Resources.ajax_loader;
            cmbUserName.Enabled = false;
            txtPassword.Enabled = false;
            btnLogin.Enabled = false;
            lblWrong.Visible = false;

            //define local variables from the user inputs 
            string user = cmbUserName.Text;
            string pass = txtPassword.Text;
            //check if eligible to be logged in 


            if (UserLogin.LoggedIn == true)
            {
                cmbUserName.Enabled = true;
                txtPassword.Enabled = true;
                cmbUserName.Text = "";
                txtPassword.Text = "";
                UserLogin.UserData = new User();
                UserLogin.LoggedIn = false;
                btnLogin.Text = "INICIAR SESIÓN";
                btnLogin.Enabled = true;

                pbxUserLogin.Image = NurseSup.Properties.Resources.user_96px;
            }
            else
            {
                int res_login = Login.login(user, pass);
                switch (res_login)
                {
                    case 1: //login success
                        cmbUserName.Enabled = false;
                        txtPassword.Enabled = false;
                        btnLogin.Text = "CERRAR SESIÓN";
                        btnLogin.Enabled = true;

                        pbxWrongUser.Visible = false;
                        pbxWrongPassword.Visible = false;
                        lblWrong.Visible = true;
                        lblWrong.Text = "Sesión de " + UserLogin.UserData.UserName + "\n iniciada correctamente";
                        lblWrong.ForeColor = Color.DodgerBlue;
                        break;
                    case -1: //incorrect
                        cmbUserName.Enabled = true;
                        txtPassword.Enabled = true;

                        pbxWrongUser.Visible = false;
                        pbxWrongPassword.Visible = false;
                        lblWrong.Visible = true;
                        lblWrong.Text = "Contraseña o nombre de usuario incorrecto";
                        lblWrong.ForeColor = Color.Firebrick;
                        break;
                    case -2: //name empty
                        cmbUserName.Enabled = true;
                        txtPassword.Enabled = true;

                        pbxWrongUser.Visible = true;
                        pbxWrongPassword.Visible = false;
                        lblWrong.Visible = false;
                        break;
                    case -3: //pswd empty
                        cmbUserName.Enabled = true;
                        txtPassword.Enabled = true;

                        pbxWrongUser.Visible = false;
                        pbxWrongPassword.Visible = true;
                        lblWrong.Visible = false;
                        break;
                    case -4: //name invalid characters
                        cmbUserName.Enabled = true;
                        txtPassword.Enabled = true;

                        pbxWrongUser.Visible = true;
                        pbxWrongPassword.Visible = false;
                        lblWrong.Visible = false;
                        break;
                    default:
                        break;
                }
                btnLogin.Enabled = true;
                pbxUserLogin.Image = NurseSup.Properties.Resources.user_96px;
            }
        }

        private void cmbUserName_TextChanged(object sender, EventArgs e)
        {
            pbxWrongUser.Visible = false;
            pbxWrongPassword.Visible = false;
        }

        private void txtPassword_TextChanged(object sender, EventArgs e)
        {
            pbxWrongUser.Visible = false;
            pbxWrongPassword.Visible = false;
        }

        private void ucLogin_VisibleChanged(object sender, EventArgs e)
        {
            if (this.Visible == true)
            {
                ParentForm.AcceptButton = btnLogin;
            }
        }

        private void cmbUserName_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void cmbUserName_DrawItem(object sender, DrawItemEventArgs e)
        {
            ComboBox box = sender as ComboBox;

            if (Object.ReferenceEquals(null, box))
                return;

            e.DrawBackground();

            if (e.Index >= 0)
            {
                Graphics g = e.Graphics;

                using (Brush brush = ((e.State & DrawItemState.Selected) == DrawItemState.Selected)
                                      ? new SolidBrush(SystemColors.Highlight)
                                      : new SolidBrush(e.BackColor))
                {
                    using (Brush textBrush = new SolidBrush(e.ForeColor))
                    {
                        g.FillRectangle(brush, e.Bounds);

                        Pen pen = new Pen(Color.LightGray, 2);
                        pen.Alignment = PenAlignment.Inset; //<-- this
                        g.DrawRectangle(pen, e.Bounds);

                        DataRow row = dtUsers.Rows[e.Index];
                        string texto = row["UserName"].ToString();

                        StringFormat format = StringFormat.GenericDefault;
                        format.LineAlignment = StringAlignment.Center;
                        //format.Alignment = StringAlignment.Center;

                        g.DrawString(texto,
                                     e.Font,
                                     textBrush,
                                     e.Bounds,
                                     format);
                    }
                }
            }

            e.DrawFocusRectangle();
        }
    }
}
