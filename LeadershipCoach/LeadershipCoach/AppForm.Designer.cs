﻿namespace NurseSup
{
    partial class AppForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AppForm));
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanelMenuHide = new System.Windows.Forms.TableLayoutPanel();
            this.btnLoginS = new System.Windows.Forms.Button();
            this.btnConfiguraciónS = new System.Windows.Forms.Button();
            this.btnHistorialS = new System.Windows.Forms.Button();
            this.btnGameS = new System.Windows.Forms.Button();
            this.btnExpandir = new System.Windows.Forms.Button();
            this.btnHomeS = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.tableLayoutPanelMenuShow = new System.Windows.Forms.TableLayoutPanel();
            this.btnLogin = new System.Windows.Forms.Button();
            this.btnContraer = new System.Windows.Forms.Button();
            this.btnConfiguración = new System.Windows.Forms.Button();
            this.btnHistorial = new System.Windows.Forms.Button();
            this.btnHome = new System.Windows.Forms.Button();
            this.pbxLogoUnilever = new System.Windows.Forms.PictureBox();
            this.btnJuego = new System.Windows.Forms.Button();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.PanelMain = new System.Windows.Forms.Panel();
            this.panelInicio = new System.Windows.Forms.Panel();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.tmrLogin = new System.Windows.Forms.Timer(this.components);
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.btnHabilidad1 = new System.Windows.Forms.Button();
            this.btnHabilidad2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanelMenuHide.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.tableLayoutPanelMenuShow.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxLogoUnilever)).BeginInit();
            this.tableLayoutPanel2.SuspendLayout();
            this.PanelMain.SuspendLayout();
            this.panelInicio.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 61F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 214F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanelMenuHide, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanelMenuShow, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 2, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 591F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1383, 998);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // tableLayoutPanelMenuHide
            // 
            this.tableLayoutPanelMenuHide.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(36)))), ((int)(((byte)(95)))));
            this.tableLayoutPanelMenuHide.ColumnCount = 1;
            this.tableLayoutPanelMenuHide.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelMenuHide.Controls.Add(this.btnLoginS, 0, 5);
            this.tableLayoutPanelMenuHide.Controls.Add(this.btnConfiguraciónS, 0, 2);
            this.tableLayoutPanelMenuHide.Controls.Add(this.btnHistorialS, 0, 3);
            this.tableLayoutPanelMenuHide.Controls.Add(this.btnGameS, 0, 3);
            this.tableLayoutPanelMenuHide.Controls.Add(this.btnExpandir, 0, 7);
            this.tableLayoutPanelMenuHide.Controls.Add(this.btnHomeS, 0, 1);
            this.tableLayoutPanelMenuHide.Controls.Add(this.pictureBox1, 0, 0);
            this.tableLayoutPanelMenuHide.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelMenuHide.Location = new System.Drawing.Point(4, 4);
            this.tableLayoutPanelMenuHide.Margin = new System.Windows.Forms.Padding(4);
            this.tableLayoutPanelMenuHide.Name = "tableLayoutPanelMenuHide";
            this.tableLayoutPanelMenuHide.RowCount = 8;
            this.tableLayoutPanelMenuHide.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 62F));
            this.tableLayoutPanelMenuHide.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 62F));
            this.tableLayoutPanelMenuHide.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 62F));
            this.tableLayoutPanelMenuHide.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 62F));
            this.tableLayoutPanelMenuHide.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 62F));
            this.tableLayoutPanelMenuHide.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 62F));
            this.tableLayoutPanelMenuHide.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelMenuHide.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 62F));
            this.tableLayoutPanelMenuHide.Size = new System.Drawing.Size(53, 990);
            this.tableLayoutPanelMenuHide.TabIndex = 2;
            // 
            // btnLoginS
            // 
            this.btnLoginS.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(36)))), ((int)(((byte)(95)))));
            this.btnLoginS.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnLoginS.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnLoginS.FlatAppearance.BorderSize = 0;
            this.btnLoginS.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DodgerBlue;
            this.btnLoginS.FlatAppearance.MouseOverBackColor = System.Drawing.Color.RoyalBlue;
            this.btnLoginS.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLoginS.Font = new System.Drawing.Font("Comic Sans MS", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLoginS.ForeColor = System.Drawing.Color.White;
            this.btnLoginS.Image = global::NurseSup.Properties.Resources.locked_24px;
            this.btnLoginS.Location = new System.Drawing.Point(3, 312);
            this.btnLoginS.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnLoginS.Name = "btnLoginS";
            this.btnLoginS.Size = new System.Drawing.Size(47, 58);
            this.btnLoginS.TabIndex = 16;
            this.btnLoginS.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnLoginS.UseVisualStyleBackColor = false;
            this.btnLoginS.Click += new System.EventHandler(this.btnLogin_Click);
            // 
            // btnConfiguraciónS
            // 
            this.btnConfiguraciónS.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(36)))), ((int)(((byte)(95)))));
            this.btnConfiguraciónS.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnConfiguraciónS.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnConfiguraciónS.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DodgerBlue;
            this.btnConfiguraciónS.FlatAppearance.MouseOverBackColor = System.Drawing.Color.RoyalBlue;
            this.btnConfiguraciónS.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnConfiguraciónS.ForeColor = System.Drawing.Color.White;
            this.btnConfiguraciónS.Image = global::NurseSup.Properties.Resources.mario_random;
            this.btnConfiguraciónS.Location = new System.Drawing.Point(3, 130);
            this.btnConfiguraciónS.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
            this.btnConfiguraciónS.Name = "btnConfiguraciónS";
            this.btnConfiguraciónS.Size = new System.Drawing.Size(47, 50);
            this.btnConfiguraciónS.TabIndex = 14;
            this.btnConfiguraciónS.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnConfiguraciónS.UseVisualStyleBackColor = false;
            this.btnConfiguraciónS.Click += new System.EventHandler(this.btnRandom_Click);
            // 
            // btnHistorialS
            // 
            this.btnHistorialS.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(36)))), ((int)(((byte)(95)))));
            this.btnHistorialS.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnHistorialS.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnHistorialS.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DodgerBlue;
            this.btnHistorialS.FlatAppearance.MouseOverBackColor = System.Drawing.Color.RoyalBlue;
            this.btnHistorialS.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnHistorialS.ForeColor = System.Drawing.Color.White;
            this.btnHistorialS.Image = global::NurseSup.Properties.Resources.Historial_24px;
            this.btnHistorialS.Location = new System.Drawing.Point(3, 254);
            this.btnHistorialS.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
            this.btnHistorialS.Name = "btnHistorialS";
            this.btnHistorialS.Size = new System.Drawing.Size(47, 50);
            this.btnHistorialS.TabIndex = 13;
            this.btnHistorialS.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnHistorialS.UseVisualStyleBackColor = false;
            this.btnHistorialS.Visible = false;
            this.btnHistorialS.Click += new System.EventHandler(this.btnHistorial_Click);
            // 
            // btnGameS
            // 
            this.btnGameS.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(36)))), ((int)(((byte)(95)))));
            this.btnGameS.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnGameS.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnGameS.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DodgerBlue;
            this.btnGameS.FlatAppearance.MouseOverBackColor = System.Drawing.Color.RoyalBlue;
            this.btnGameS.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGameS.ForeColor = System.Drawing.Color.White;
            this.btnGameS.Image = global::NurseSup.Properties.Resources.question;
            this.btnGameS.Location = new System.Drawing.Point(3, 192);
            this.btnGameS.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
            this.btnGameS.Name = "btnGameS";
            this.btnGameS.Size = new System.Drawing.Size(47, 50);
            this.btnGameS.TabIndex = 12;
            this.btnGameS.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnGameS.UseVisualStyleBackColor = false;
            this.btnGameS.Click += new System.EventHandler(this.btnGame_Click);
            // 
            // btnExpandir
            // 
            this.btnExpandir.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(36)))), ((int)(((byte)(95)))));
            this.btnExpandir.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnExpandir.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnExpandir.FlatAppearance.BorderSize = 0;
            this.btnExpandir.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DodgerBlue;
            this.btnExpandir.FlatAppearance.MouseOverBackColor = System.Drawing.Color.RoyalBlue;
            this.btnExpandir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExpandir.Font = new System.Drawing.Font("Comic Sans MS", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExpandir.ForeColor = System.Drawing.Color.White;
            this.btnExpandir.Image = global::NurseSup.Properties.Resources.double_right_24px;
            this.btnExpandir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnExpandir.Location = new System.Drawing.Point(3, 930);
            this.btnExpandir.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnExpandir.Name = "btnExpandir";
            this.btnExpandir.Size = new System.Drawing.Size(47, 58);
            this.btnExpandir.TabIndex = 11;
            this.btnExpandir.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnExpandir.UseVisualStyleBackColor = false;
            this.btnExpandir.Click += new System.EventHandler(this.lblShowMenu_Click);
            // 
            // btnHomeS
            // 
            this.btnHomeS.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(36)))), ((int)(((byte)(95)))));
            this.btnHomeS.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnHomeS.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnHomeS.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DodgerBlue;
            this.btnHomeS.FlatAppearance.MouseOverBackColor = System.Drawing.Color.RoyalBlue;
            this.btnHomeS.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnHomeS.ForeColor = System.Drawing.Color.White;
            this.btnHomeS.Image = global::NurseSup.Properties.Resources.Home_24px;
            this.btnHomeS.Location = new System.Drawing.Point(3, 68);
            this.btnHomeS.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
            this.btnHomeS.Name = "btnHomeS";
            this.btnHomeS.Size = new System.Drawing.Size(47, 50);
            this.btnHomeS.TabIndex = 10;
            this.btnHomeS.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnHomeS.UseVisualStyleBackColor = false;
            this.btnHomeS.Click += new System.EventHandler(this.btnHome_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox1.Image = global::NurseSup.Properties.Resources.UNI_WHITE;
            this.pictureBox1.Location = new System.Drawing.Point(4, 4);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(45, 54);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 9;
            this.pictureBox1.TabStop = false;
            // 
            // tableLayoutPanelMenuShow
            // 
            this.tableLayoutPanelMenuShow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(36)))), ((int)(((byte)(95)))));
            this.tableLayoutPanelMenuShow.ColumnCount = 1;
            this.tableLayoutPanelMenuShow.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelMenuShow.Controls.Add(this.btnLogin, 0, 12);
            this.tableLayoutPanelMenuShow.Controls.Add(this.btnContraer, 0, 13);
            this.tableLayoutPanelMenuShow.Controls.Add(this.btnConfiguración, 0, 2);
            this.tableLayoutPanelMenuShow.Controls.Add(this.btnHome, 0, 1);
            this.tableLayoutPanelMenuShow.Controls.Add(this.pbxLogoUnilever, 0, 0);
            this.tableLayoutPanelMenuShow.Controls.Add(this.btnHistorial, 0, 10);
            this.tableLayoutPanelMenuShow.Controls.Add(this.btnJuego, 0, 9);
            this.tableLayoutPanelMenuShow.Controls.Add(this.btnHabilidad1, 0, 3);
            this.tableLayoutPanelMenuShow.Controls.Add(this.btnHabilidad2, 0, 4);
            this.tableLayoutPanelMenuShow.Controls.Add(this.button1, 0, 5);
            this.tableLayoutPanelMenuShow.Controls.Add(this.button2, 0, 6);
            this.tableLayoutPanelMenuShow.Controls.Add(this.button3, 0, 7);
            this.tableLayoutPanelMenuShow.Controls.Add(this.button4, 0, 8);
            this.tableLayoutPanelMenuShow.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelMenuShow.Location = new System.Drawing.Point(65, 4);
            this.tableLayoutPanelMenuShow.Margin = new System.Windows.Forms.Padding(4);
            this.tableLayoutPanelMenuShow.Name = "tableLayoutPanelMenuShow";
            this.tableLayoutPanelMenuShow.RowCount = 14;
            this.tableLayoutPanelMenuShow.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanelMenuShow.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 62F));
            this.tableLayoutPanelMenuShow.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 62F));
            this.tableLayoutPanelMenuShow.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 62F));
            this.tableLayoutPanelMenuShow.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 62F));
            this.tableLayoutPanelMenuShow.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 62F));
            this.tableLayoutPanelMenuShow.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 62F));
            this.tableLayoutPanelMenuShow.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 62F));
            this.tableLayoutPanelMenuShow.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 62F));
            this.tableLayoutPanelMenuShow.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 62F));
            this.tableLayoutPanelMenuShow.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 1F));
            this.tableLayoutPanelMenuShow.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelMenuShow.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 58F));
            this.tableLayoutPanelMenuShow.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 62F));
            this.tableLayoutPanelMenuShow.Size = new System.Drawing.Size(206, 990);
            this.tableLayoutPanelMenuShow.TabIndex = 1;
            // 
            // btnLogin
            // 
            this.btnLogin.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(36)))), ((int)(((byte)(95)))));
            this.btnLogin.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnLogin.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnLogin.FlatAppearance.BorderSize = 0;
            this.btnLogin.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DodgerBlue;
            this.btnLogin.FlatAppearance.MouseOverBackColor = System.Drawing.Color.RoyalBlue;
            this.btnLogin.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLogin.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.btnLogin.ForeColor = System.Drawing.Color.White;
            this.btnLogin.Image = global::NurseSup.Properties.Resources.locked_24px;
            this.btnLogin.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnLogin.Location = new System.Drawing.Point(3, 872);
            this.btnLogin.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnLogin.Name = "btnLogin";
            this.btnLogin.Size = new System.Drawing.Size(200, 54);
            this.btnLogin.TabIndex = 17;
            this.btnLogin.Text = "          Iniciar sesión";
            this.btnLogin.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnLogin.UseVisualStyleBackColor = false;
            this.btnLogin.Click += new System.EventHandler(this.btnLogin_Click);
            // 
            // btnContraer
            // 
            this.btnContraer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(36)))), ((int)(((byte)(95)))));
            this.btnContraer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnContraer.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnContraer.FlatAppearance.BorderSize = 0;
            this.btnContraer.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DodgerBlue;
            this.btnContraer.FlatAppearance.MouseOverBackColor = System.Drawing.Color.RoyalBlue;
            this.btnContraer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnContraer.Font = new System.Drawing.Font("Comic Sans MS", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnContraer.ForeColor = System.Drawing.Color.White;
            this.btnContraer.Image = global::NurseSup.Properties.Resources.double_left_24px;
            this.btnContraer.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnContraer.Location = new System.Drawing.Point(0, 928);
            this.btnContraer.Margin = new System.Windows.Forms.Padding(0);
            this.btnContraer.Name = "btnContraer";
            this.btnContraer.Size = new System.Drawing.Size(206, 62);
            this.btnContraer.TabIndex = 12;
            this.btnContraer.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnContraer.UseVisualStyleBackColor = false;
            this.btnContraer.Click += new System.EventHandler(this.lblHideMenu_Click);
            // 
            // btnConfiguración
            // 
            this.btnConfiguración.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(36)))), ((int)(((byte)(95)))));
            this.btnConfiguración.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnConfiguración.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DodgerBlue;
            this.btnConfiguración.FlatAppearance.MouseOverBackColor = System.Drawing.Color.RoyalBlue;
            this.btnConfiguración.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnConfiguración.ForeColor = System.Drawing.Color.White;
            this.btnConfiguración.Image = global::NurseSup.Properties.Resources.mario_random;
            this.btnConfiguración.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnConfiguración.Location = new System.Drawing.Point(7, 268);
            this.btnConfiguración.Margin = new System.Windows.Forms.Padding(7, 6, 7, 6);
            this.btnConfiguración.Name = "btnConfiguración";
            this.btnConfiguración.Size = new System.Drawing.Size(192, 50);
            this.btnConfiguración.TabIndex = 11;
            this.btnConfiguración.Text = "          Ponte a prueba con\r\n          preguntas aleatorias";
            this.btnConfiguración.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnConfiguración.UseVisualStyleBackColor = false;
            this.btnConfiguración.Click += new System.EventHandler(this.btnRandom_Click);
            // 
            // btnHistorial
            // 
            this.btnHistorial.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(36)))), ((int)(((byte)(95)))));
            this.btnHistorial.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnHistorial.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DodgerBlue;
            this.btnHistorial.FlatAppearance.MouseOverBackColor = System.Drawing.Color.RoyalBlue;
            this.btnHistorial.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnHistorial.ForeColor = System.Drawing.Color.White;
            this.btnHistorial.Image = global::NurseSup.Properties.Resources.Historial_24px;
            this.btnHistorial.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnHistorial.Location = new System.Drawing.Point(7, 764);
            this.btnHistorial.Margin = new System.Windows.Forms.Padding(7, 6, 7, 6);
            this.btnHistorial.Name = "btnHistorial";
            this.btnHistorial.Size = new System.Drawing.Size(192, 1);
            this.btnHistorial.TabIndex = 10;
            this.btnHistorial.Text = "          Historial y puntajes";
            this.btnHistorial.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnHistorial.UseVisualStyleBackColor = false;
            this.btnHistorial.Visible = false;
            this.btnHistorial.Click += new System.EventHandler(this.btnHistorial_Click);
            // 
            // btnHome
            // 
            this.btnHome.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(36)))), ((int)(((byte)(95)))));
            this.btnHome.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnHome.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnHome.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DodgerBlue;
            this.btnHome.FlatAppearance.MouseOverBackColor = System.Drawing.Color.RoyalBlue;
            this.btnHome.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnHome.ForeColor = System.Drawing.Color.White;
            this.btnHome.Image = global::NurseSup.Properties.Resources.Home_24px;
            this.btnHome.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnHome.Location = new System.Drawing.Point(7, 206);
            this.btnHome.Margin = new System.Windows.Forms.Padding(7, 6, 7, 6);
            this.btnHome.Name = "btnHome";
            this.btnHome.Size = new System.Drawing.Size(192, 50);
            this.btnHome.TabIndex = 9;
            this.btnHome.Text = "          Inicio";
            this.btnHome.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnHome.UseVisualStyleBackColor = false;
            this.btnHome.Click += new System.EventHandler(this.btnHome_Click);
            // 
            // pbxLogoUnilever
            // 
            this.pbxLogoUnilever.BackColor = System.Drawing.Color.White;
            this.pbxLogoUnilever.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pbxLogoUnilever.Image = global::NurseSup.Properties.Resources.UNI_WHITE;
            this.pbxLogoUnilever.Location = new System.Drawing.Point(4, 4);
            this.pbxLogoUnilever.Margin = new System.Windows.Forms.Padding(4);
            this.pbxLogoUnilever.Name = "pbxLogoUnilever";
            this.pbxLogoUnilever.Size = new System.Drawing.Size(198, 192);
            this.pbxLogoUnilever.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbxLogoUnilever.TabIndex = 2;
            this.pbxLogoUnilever.TabStop = false;
            // 
            // btnJuego
            // 
            this.btnJuego.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(36)))), ((int)(((byte)(95)))));
            this.btnJuego.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnJuego.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DodgerBlue;
            this.btnJuego.FlatAppearance.MouseOverBackColor = System.Drawing.Color.RoyalBlue;
            this.btnJuego.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnJuego.ForeColor = System.Drawing.Color.White;
            this.btnJuego.Image = global::NurseSup.Properties.Resources.question;
            this.btnJuego.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnJuego.Location = new System.Drawing.Point(7, 702);
            this.btnJuego.Margin = new System.Windows.Forms.Padding(7, 6, 7, 6);
            this.btnJuego.Name = "btnJuego";
            this.btnJuego.Size = new System.Drawing.Size(192, 50);
            this.btnJuego.TabIndex = 8;
            this.btnJuego.Text = "          Modo Historia";
            this.btnJuego.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.toolTip1.SetToolTip(this.btnJuego, "Próximamente");
            this.btnJuego.UseVisualStyleBackColor = false;
            this.btnJuego.Click += new System.EventHandler(this.btnGame_Click);
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.PanelMain, 0, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(275, 0);
            this.tableLayoutPanel2.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(1108, 998);
            this.tableLayoutPanel2.TabIndex = 3;
            // 
            // PanelMain
            // 
            this.PanelMain.Controls.Add(this.panelInicio);
            this.PanelMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PanelMain.Location = new System.Drawing.Point(0, 0);
            this.PanelMain.Margin = new System.Windows.Forms.Padding(0);
            this.PanelMain.Name = "PanelMain";
            this.PanelMain.Size = new System.Drawing.Size(1108, 998);
            this.PanelMain.TabIndex = 0;
            // 
            // panelInicio
            // 
            this.panelInicio.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(94)))), ((int)(((byte)(156)))));
            this.panelInicio.Controls.Add(this.tableLayoutPanel3);
            this.panelInicio.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelInicio.Location = new System.Drawing.Point(0, 0);
            this.panelInicio.Margin = new System.Windows.Forms.Padding(4);
            this.panelInicio.Name = "panelInicio";
            this.panelInicio.Size = new System.Drawing.Size(1108, 998);
            this.panelInicio.TabIndex = 0;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 3;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5.555555F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 88.88889F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5.555555F));
            this.tableLayoutPanel3.Controls.Add(this.panel1, 1, 1);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel3.Margin = new System.Windows.Forms.Padding(4);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 3;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.555555F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 88.88889F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.555555F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(1108, 998);
            this.tableLayoutPanel3.TabIndex = 0;
            // 
            // panel1
            // 
            this.panel1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel1.BackgroundImage")));
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(65, 59);
            this.panel1.Margin = new System.Windows.Forms.Padding(4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(976, 879);
            this.panel1.TabIndex = 0;
            // 
            // tmrLogin
            // 
            this.tmrLogin.Enabled = true;
            this.tmrLogin.Interval = 10;
            this.tmrLogin.Tick += new System.EventHandler(this.tmrLogin_Tick);
            // 
            // btnHabilidad1
            // 
            this.btnHabilidad1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(36)))), ((int)(((byte)(95)))));
            this.btnHabilidad1.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnHabilidad1.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DodgerBlue;
            this.btnHabilidad1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.RoyalBlue;
            this.btnHabilidad1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnHabilidad1.ForeColor = System.Drawing.Color.White;
            this.btnHabilidad1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnHabilidad1.Location = new System.Drawing.Point(7, 330);
            this.btnHabilidad1.Margin = new System.Windows.Forms.Padding(7, 6, 7, 6);
            this.btnHabilidad1.Name = "btnHabilidad1";
            this.btnHabilidad1.Size = new System.Drawing.Size(192, 50);
            this.btnHabilidad1.TabIndex = 11;
            this.btnHabilidad1.Text = "Preguntas aleatorias\r\nH1: Comunicación";
            this.btnHabilidad1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnHabilidad1.UseVisualStyleBackColor = false;
            this.btnHabilidad1.Click += new System.EventHandler(this.btnHabilidad1_Click);
            // 
            // btnHabilidad2
            // 
            this.btnHabilidad2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(36)))), ((int)(((byte)(95)))));
            this.btnHabilidad2.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnHabilidad2.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DodgerBlue;
            this.btnHabilidad2.FlatAppearance.MouseOverBackColor = System.Drawing.Color.RoyalBlue;
            this.btnHabilidad2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnHabilidad2.ForeColor = System.Drawing.Color.White;
            this.btnHabilidad2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnHabilidad2.Location = new System.Drawing.Point(7, 392);
            this.btnHabilidad2.Margin = new System.Windows.Forms.Padding(7, 6, 7, 6);
            this.btnHabilidad2.Name = "btnHabilidad2";
            this.btnHabilidad2.Size = new System.Drawing.Size(192, 50);
            this.btnHabilidad2.TabIndex = 11;
            this.btnHabilidad2.Text = "Preguntas aleatorias\r\nH2: Trabajo en equipo\r\n";
            this.btnHabilidad2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnHabilidad2.UseVisualStyleBackColor = false;
            this.btnHabilidad2.Click += new System.EventHandler(this.btnHabilidad2_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(36)))), ((int)(((byte)(95)))));
            this.button1.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button1.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DodgerBlue;
            this.button1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.RoyalBlue;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button1.Location = new System.Drawing.Point(7, 454);
            this.button1.Margin = new System.Windows.Forms.Padding(7, 6, 7, 6);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(192, 50);
            this.button1.TabIndex = 11;
            this.button1.Text = "Preguntas aleatorias\r\nH3: Responsabilidad";
            this.button1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.toolTip1.SetToolTip(this.button1, "Próximamente");
            this.button1.UseVisualStyleBackColor = false;
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(36)))), ((int)(((byte)(95)))));
            this.button2.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button2.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DodgerBlue;
            this.button2.FlatAppearance.MouseOverBackColor = System.Drawing.Color.RoyalBlue;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.ForeColor = System.Drawing.Color.White;
            this.button2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button2.Location = new System.Drawing.Point(7, 516);
            this.button2.Margin = new System.Windows.Forms.Padding(7, 6, 7, 6);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(192, 50);
            this.button2.TabIndex = 11;
            this.button2.Text = "Preguntas aleatorias\r\nH4: Empatía";
            this.button2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.toolTip1.SetToolTip(this.button2, "Próximamente");
            this.button2.UseVisualStyleBackColor = false;
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(36)))), ((int)(((byte)(95)))));
            this.button3.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button3.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DodgerBlue;
            this.button3.FlatAppearance.MouseOverBackColor = System.Drawing.Color.RoyalBlue;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.ForeColor = System.Drawing.Color.White;
            this.button3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button3.Location = new System.Drawing.Point(7, 578);
            this.button3.Margin = new System.Windows.Forms.Padding(7, 6, 7, 6);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(192, 50);
            this.button3.TabIndex = 11;
            this.button3.Text = "Preguntas aleatorias\r\nH5: Proactividad";
            this.button3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.toolTip1.SetToolTip(this.button3, "Próximamente");
            this.button3.UseVisualStyleBackColor = false;
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(36)))), ((int)(((byte)(95)))));
            this.button4.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button4.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DodgerBlue;
            this.button4.FlatAppearance.MouseOverBackColor = System.Drawing.Color.RoyalBlue;
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button4.ForeColor = System.Drawing.Color.White;
            this.button4.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button4.Location = new System.Drawing.Point(7, 640);
            this.button4.Margin = new System.Windows.Forms.Padding(7, 6, 7, 6);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(192, 50);
            this.button4.TabIndex = 11;
            this.button4.Text = "Preguntas aleatorias\r\nH6: Compromiso";
            this.button4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.toolTip1.SetToolTip(this.button4, "Próximamente");
            this.button4.UseVisualStyleBackColor = false;
            // 
            // AppForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1383, 998);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "AppForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "NurseSup+";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.AppForm_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanelMenuHide.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.tableLayoutPanelMenuShow.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbxLogoUnilever)).EndInit();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.PanelMain.ResumeLayout(false);
            this.panelInicio.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelMenuHide;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelMenuShow;
        private System.Windows.Forms.PictureBox pbxLogoUnilever;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button btnJuego;
        private System.Windows.Forms.Button btnHomeS;
        private System.Windows.Forms.Button btnHistorial;
        private System.Windows.Forms.Button btnHome;
        private System.Windows.Forms.Button btnExpandir;
        private System.Windows.Forms.Button btnContraer;
        private System.Windows.Forms.Button btnGameS;
        private System.Windows.Forms.Button btnHistorialS;
        private System.Windows.Forms.Button btnConfiguraciónS;
        private System.Windows.Forms.Button btnConfiguración;
        private System.Windows.Forms.Button btnLoginS;
        private System.Windows.Forms.Button btnLogin;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.Timer tmrLogin;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Panel PanelMain;
        private System.Windows.Forms.Panel panelInicio;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Button btnHabilidad1;
        private System.Windows.Forms.Button btnHabilidad2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
    }
}

