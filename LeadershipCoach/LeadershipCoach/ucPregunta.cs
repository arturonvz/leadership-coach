﻿using ingeniaCore;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NurseSup
{
    public partial class ucPregunta : UserControl
    {
        bool PreguntaRespondida = false;
        public string NameRadioButtonSelected;
        string FiltroPreguntas = "id > 1";
        public RadioButton RadioButtonCorrect = new RadioButton();
        PreguntaItem Pregunta;

        Habilidad HabilidadPreguntaActual = null;
        Competencia CompetenciaPreguntaActual = null;

        Opciones[] OpcionesRespuestasDesordenadas = new Opciones[4];
        class Opciones
        {
            public RadioButton HandlerRadioButton; // Cambiado a public para acceder desde el formulario
            public string Respuesta;
            public bool EsCorrecta;
        }

        public ucPregunta()
        {
            InitializeComponent();
        }

        public ucPregunta(PreguntaItem pregunta)
        {
            InitializeComponent();
            ActualizarPregunta(pregunta);
        }

        public ucPregunta(PreguntaItem pregunta, string filtroPreguntas)
        {
            FiltroPreguntas = filtroPreguntas;
            InitializeComponent();
            ActualizarPregunta(pregunta);
        }

        private void ActualizarPregunta(PreguntaItem pregunta)
        {
            Pregunta = pregunta;

            // Obtener la Habilidad asociada a la pregunta
            SQLResponse habilidadResponse = Server.SQLDatabase.ObtenerTabla("*", "id = " + Pregunta.IdHabilidad, "Habilidades", "id");

            if (habilidadResponse.Count >= 1)
            {
                DataRow habilidadRow = habilidadResponse.DataTable.Rows[0];
                HabilidadPreguntaActual = new Habilidad(habilidadRow);
            }

            // Obtener la Competencia asociada a la pregunta (usando CodigoCompetencia de HabilidadPreguntaActual)
            if (HabilidadPreguntaActual != null)
            {
                SQLResponse competenciaResponse = Server.SQLDatabase.ObtenerTabla("*", "CódigoCompetencia = '" + HabilidadPreguntaActual.CodigoCompetencia + "'", "Competencias", "id");

                if (competenciaResponse.Count >= 1)
                {
                    DataRow competenciaRow = competenciaResponse.DataTable.Rows[0];
                    CompetenciaPreguntaActual = new Competencia(competenciaRow);
                }
            }
            // Verificar si CompetenciaPreguntaActual y HabilidadPreguntaActual no son nulos
            if (CompetenciaPreguntaActual != null && HabilidadPreguntaActual != null)
            {
                // Concatenar los valores y asignar al Text del lblCompetenciaYHabilidad
                lblCompetenciaYHabilidad.Text = $"Competencia: {CompetenciaPreguntaActual.NombreCompetencia}\nHabilidad: {HabilidadPreguntaActual.NombreHabilidad}";
            }
            else
            {
                // En caso de que alguno sea nulo, puedes asignar un mensaje de error o manejarlo según tus necesidades
                lblCompetenciaYHabilidad.Text = "Error: No se encontró Competencia o Habilidad";
            }

            btnNext.Enabled = false;
            btnNext.BackColor = Color.Transparent;
            panel1.Enabled = true;
            panel2.Enabled = true;
            panel3.Enabled = true;
            panel4.Enabled = true;

            lblPregunta.Text = pregunta.Pregunta;
            

            radioButton1.Text = "A)  ";
            radioButton2.Text = "B)  ";
            radioButton3.Text = "C)  ";
            radioButton4.Text = "D)  ";

            // Llenar el array OpcionesRespuestasDesordenadas
            OpcionesRespuestasDesordenadas[0] = new Opciones { HandlerRadioButton = radioButton1, Respuesta = pregunta.Respuesta, EsCorrecta = true };
            OpcionesRespuestasDesordenadas[1] = new Opciones { HandlerRadioButton = radioButton2, Respuesta = pregunta.RespuestaFalsa1, EsCorrecta = false };
            OpcionesRespuestasDesordenadas[2] = new Opciones { HandlerRadioButton = radioButton3, Respuesta = pregunta.RespuestaFalsa2, EsCorrecta = false };
            OpcionesRespuestasDesordenadas[3] = new Opciones { HandlerRadioButton = radioButton4, Respuesta = pregunta.RespuestaFalsa3, EsCorrecta = false };

            // Desordenar las opciones de respuesta
            Random random = new Random();
            OpcionesRespuestasDesordenadas = OpcionesRespuestasDesordenadas.OrderBy(x => random.Next()).ToArray();

            // Configurar los textos de los RadioButton
            radioButton1.Text += OpcionesRespuestasDesordenadas[0].Respuesta;
            radioButton2.Text += OpcionesRespuestasDesordenadas[1].Respuesta;
            radioButton3.Text += OpcionesRespuestasDesordenadas[2].Respuesta;
            radioButton4.Text += OpcionesRespuestasDesordenadas[3].Respuesta;

            // Limpiar la selección de todos los RadioButton
            radioButton1.Checked = false;
            radioButton2.Checked = false;
            radioButton3.Checked = false;
            radioButton4.Checked = false;

            // Actualizar los Handlers después de desordenar
            OpcionesRespuestasDesordenadas[0].HandlerRadioButton = radioButton1;
            OpcionesRespuestasDesordenadas[1].HandlerRadioButton = radioButton2;
            OpcionesRespuestasDesordenadas[2].HandlerRadioButton = radioButton3;
            OpcionesRespuestasDesordenadas[3].HandlerRadioButton = radioButton4;

            // Aplicar los colores de fondo
            radioButton1.FlatAppearance.CheckedBackColor = OpcionesRespuestasDesordenadas[0].EsCorrecta ? Color.Green : Color.Red;
            radioButton2.FlatAppearance.CheckedBackColor = OpcionesRespuestasDesordenadas[1].EsCorrecta ? Color.Green : Color.Red;
            radioButton3.FlatAppearance.CheckedBackColor = OpcionesRespuestasDesordenadas[2].EsCorrecta ? Color.Green : Color.Red;
            radioButton4.FlatAppearance.CheckedBackColor = OpcionesRespuestasDesordenadas[3].EsCorrecta ? Color.Green : Color.Red;

            PreguntaRespondida = false;
            NameRadioButtonSelected = "";

            // Obtener el nombre del RadioButton que contiene la opción correcta
            RadioButton HandlerRadioButtonCorrect = OpcionesRespuestasDesordenadas.First(opcion => opcion.EsCorrecta).HandlerRadioButton;

            // Guardar el nombre en una variable para su posterior uso
            RadioButtonCorrect = HandlerRadioButtonCorrect;
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            // Llamar al método para actualizar la pregunta
            ActualizarPregunta(NuevaPreguntaDesdeLaBaseDeDatos());// Restablecer el color de fondo predeterminado
        }

        private PreguntaItem NuevaPreguntaDesdeLaBaseDeDatos()
        {
            // Obtener datos de la tabla "Preguntas" con condición "id > 1" y ordenados por "id"
            SQLResponse sQLResponse = Server.SQLDatabase.ObtenerTabla("*", FiltroPreguntas, "Preguntas", "id");

            if (sQLResponse.Count >= 1)
            {
                // Obtener un número aleatorio entre 0 y el número de filas - 1
                Random random = new Random();
                int randomIndex = random.Next(0, sQLResponse.DataTable.Rows.Count);

                // Seleccionar la fila aleatoria
                DataRow drRandom = sQLResponse.DataTable.Rows[randomIndex];

                // Retornar una nueva instancia de PreguntaItem
                return new PreguntaItem(drRandom);
            }

            // En caso de no encontrar una pregunta, podrías retornar null o lanzar una excepción según tus necesidades.
            return null;
        }


        private void radioButton_CheckedChanged(object sender, EventArgs e)
        {
            RadioButton selectedRadioButton = (RadioButton)sender;

            // Verificar si el RadioButton está marcado
            if (selectedRadioButton.Checked)
            {
                if (!PreguntaRespondida)
                {
                    PreguntaRespondida = true;
                    NameRadioButtonSelected = selectedRadioButton.Name;
                    // Verificar la respuesta seleccionada
                    bool esCorrecta = OpcionesRespuestasDesordenadas.First(opcion => opcion.HandlerRadioButton == selectedRadioButton).EsCorrecta;

                    if (esCorrecta)
                    {
                        int valorActual = int.Parse(btnCorrectas.Text);
                        btnCorrectas.Text = (valorActual + 1).ToString();
                    }
                    else
                    {
                        int valorActual = int.Parse(btnIncorrectas.Text);
                        btnIncorrectas.Text = (valorActual + 1).ToString();
                        RadioButtonCorrect.Checked = true;
                    }

                    // Deshabilitar todos los RadioButtons
                    panel1.Enabled = false;
                    panel2.Enabled = false;
                    panel3.Enabled = false;
                    panel4.Enabled = false;

                    // Activar tu botón Next dependiendo de si la respuesta fue correcta o no
                    btnNext.BackColor = esCorrecta ? Color.Green : Color.Red;
                    btnNext.Enabled = true;
                }
                else
                {
                    if (NameRadioButtonSelected != selectedRadioButton.Name && RadioButtonCorrect.Name != selectedRadioButton.Name)
                    {
                        selectedRadioButton.Checked = false;
                    }
                }
            }
        }

    }
}