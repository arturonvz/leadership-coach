﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NurseSup
{
    public class User
    {
        public long Id { get; set; }
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Department { get; set; }
        public string Position { get; set; }
        public string Password { get; set; }
        public byte[] HashedPassword { get; set; }
        public long? IdGrupo { get; set; }
        public string Code { get; set; }
        public string AppInfo { get; set; }

        public User()
        {
            // Constructor sin argumentos
        }

        public User(DataRow row)
        {
            Id = row.Field<long>("id");
            UserName = row.Field<string>("UserName");
            FirstName = row.Field<string>("FirstName");
            LastName = row.Field<string>("LastName");
            Department = row.Field<string>("Department");
            Position = row.Field<string>("Position");
            Password = row.Field<string>("Password");
            HashedPassword = row.Field<byte[]>("HashedPassword");
            IdGrupo = row.Field<long?>("idGrupo");
            Code = row.Field<string>("Code");
            AppInfo = row.Field<string>("AppInfo");
        }

        public void UpdateFromDataRow(DataRow row)
        {
            Id = row.Field<long>("id");
            UserName = row.Field<string>("UserName");
            FirstName = row.Field<string>("FirstName");
            LastName = row.Field<string>("LastName");
            Department = row.Field<string>("Department");
            Position = row.Field<string>("Position");
            Password = row.Field<string>("Password");
            HashedPassword = row.Field<byte[]>("HashedPassword");
            IdGrupo = row.Field<long?>("idGrupo");
            Code = row.Field<string>("Code");
            AppInfo = row.Field<string>("AppInfo");
        }
    }

}
