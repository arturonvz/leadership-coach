﻿namespace NurseSup
{
    partial class ucLogin
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pbxWrongPassword = new System.Windows.Forms.PictureBox();
            this.pbxWrongUser = new System.Windows.Forms.PictureBox();
            this.panelLogin = new System.Windows.Forms.Panel();
            this.btnLogin = new System.Windows.Forms.Button();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.lblWrong = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.pbxUserLogin = new System.Windows.Forms.PictureBox();
            this.cmbUserName = new System.Windows.Forms.ComboBox();
            this.usuariosBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.tmrWrongPassword = new System.Windows.Forms.Timer(this.components);
            this.tableLayoutPanel1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxWrongPassword)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxWrongUser)).BeginInit();
            this.panelLogin.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxUserLogin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.usuariosBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.panel1, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(583, 336);
            this.tableLayoutPanel1.TabIndex = 3;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.pbxWrongPassword);
            this.panel1.Controls.Add(this.pbxWrongUser);
            this.panel1.Controls.Add(this.panelLogin);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(583, 336);
            this.panel1.TabIndex = 1;
            // 
            // pbxWrongPassword
            // 
            this.pbxWrongPassword.Image = global::NurseSup.Properties.Resources.WrongField_Password;
            this.pbxWrongPassword.Location = new System.Drawing.Point(5, 162);
            this.pbxWrongPassword.Name = "pbxWrongPassword";
            this.pbxWrongPassword.Size = new System.Drawing.Size(200, 36);
            this.pbxWrongPassword.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbxWrongPassword.TabIndex = 0;
            this.pbxWrongPassword.TabStop = false;
            this.pbxWrongPassword.Visible = false;
            // 
            // pbxWrongUser
            // 
            this.pbxWrongUser.Image = global::NurseSup.Properties.Resources.WrongField_Name;
            this.pbxWrongUser.Location = new System.Drawing.Point(5, 120);
            this.pbxWrongUser.Name = "pbxWrongUser";
            this.pbxWrongUser.Size = new System.Drawing.Size(200, 36);
            this.pbxWrongUser.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbxWrongUser.TabIndex = 0;
            this.pbxWrongUser.TabStop = false;
            this.pbxWrongUser.Visible = false;
            // 
            // panelLogin
            // 
            this.panelLogin.Controls.Add(this.btnLogin);
            this.panelLogin.Controls.Add(this.txtPassword);
            this.panelLogin.Controls.Add(this.label2);
            this.panelLogin.Controls.Add(this.lblWrong);
            this.panelLogin.Controls.Add(this.label1);
            this.panelLogin.Controls.Add(this.pbxUserLogin);
            this.panelLogin.Controls.Add(this.cmbUserName);
            this.panelLogin.Location = new System.Drawing.Point(145, 0);
            this.panelLogin.Margin = new System.Windows.Forms.Padding(0);
            this.panelLogin.Name = "panelLogin";
            this.panelLogin.Size = new System.Drawing.Size(339, 333);
            this.panelLogin.TabIndex = 1;
            // 
            // btnLogin
            // 
            this.btnLogin.BackColor = System.Drawing.Color.SteelBlue;
            this.btnLogin.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLogin.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLogin.ForeColor = System.Drawing.Color.White;
            this.btnLogin.Location = new System.Drawing.Point(60, 235);
            this.btnLogin.Name = "btnLogin";
            this.btnLogin.Size = new System.Drawing.Size(219, 29);
            this.btnLogin.TabIndex = 2;
            this.btnLogin.Text = "INICIAR SESIÓN";
            this.btnLogin.UseVisualStyleBackColor = false;
            this.btnLogin.Click += new System.EventHandler(this.btnLogin_Click);
            // 
            // txtPassword
            // 
            this.txtPassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPassword.Location = new System.Drawing.Point(60, 196);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.PasswordChar = '*';
            this.txtPassword.Size = new System.Drawing.Size(219, 26);
            this.txtPassword.TabIndex = 1;
            this.txtPassword.TextChanged += new System.EventHandler(this.txtPassword_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(57, 181);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(78, 14);
            this.label2.TabIndex = 1;
            this.label2.Text = "CONTRASEÑA";
            // 
            // lblWrong
            // 
            this.lblWrong.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblWrong.ForeColor = System.Drawing.Color.Firebrick;
            this.lblWrong.Location = new System.Drawing.Point(43, 267);
            this.lblWrong.Name = "lblWrong";
            this.lblWrong.Size = new System.Drawing.Size(250, 32);
            this.lblWrong.TabIndex = 1;
            this.lblWrong.Text = "Contraseña o nombre de usuario incorrecto";
            this.lblWrong.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.lblWrong.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(57, 112);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(115, 14);
            this.label1.TabIndex = 1;
            this.label1.Text = "NOMBRE DE USUARIO";
            // 
            // pbxUserLogin
            // 
            this.pbxUserLogin.Image = global::NurseSup.Properties.Resources.user_96px;
            this.pbxUserLogin.Location = new System.Drawing.Point(120, 3);
            this.pbxUserLogin.Name = "pbxUserLogin";
            this.pbxUserLogin.Size = new System.Drawing.Size(98, 100);
            this.pbxUserLogin.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pbxUserLogin.TabIndex = 0;
            this.pbxUserLogin.TabStop = false;
            // 
            // cmbUserName
            // 
            this.cmbUserName.DataSource = this.usuariosBindingSource;
            this.cmbUserName.DisplayMember = "UserName";
            this.cmbUserName.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cmbUserName.DropDownHeight = 450;
            this.cmbUserName.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.cmbUserName.FormattingEnabled = true;
            this.cmbUserName.IntegralHeight = false;
            this.cmbUserName.ItemHeight = 40;
            this.cmbUserName.Location = new System.Drawing.Point(60, 128);
            this.cmbUserName.Name = "cmbUserName";
            this.cmbUserName.Size = new System.Drawing.Size(219, 46);
            this.cmbUserName.TabIndex = 0;
            this.cmbUserName.ValueMember = "UserName";
            this.cmbUserName.DrawItem += new System.Windows.Forms.DrawItemEventHandler(this.cmbUserName_DrawItem);
            this.cmbUserName.SelectedIndexChanged += new System.EventHandler(this.cmbUserName_SelectedIndexChanged);
            this.cmbUserName.TextChanged += new System.EventHandler(this.cmbUserName_TextChanged);
            // 
            // ucLogin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "ucLogin";
            this.Size = new System.Drawing.Size(583, 336);
            this.Load += new System.EventHandler(this.ucLogin_Load);
            this.VisibleChanged += new System.EventHandler(this.ucLogin_VisibleChanged);
            this.Resize += new System.EventHandler(this.ucLogin_Resize);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbxWrongPassword)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxWrongUser)).EndInit();
            this.panelLogin.ResumeLayout(false);
            this.panelLogin.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxUserLogin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.usuariosBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox pbxUserLogin;
        private System.Windows.Forms.Panel panelLogin;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.Button btnLogin;
        private System.Windows.Forms.Label lblWrong;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Timer tmrWrongPassword;
        private System.Windows.Forms.PictureBox pbxWrongUser;
        private System.Windows.Forms.PictureBox pbxWrongPassword;
        private System.Windows.Forms.ComboBox cmbUserName;
        private System.Windows.Forms.BindingSource usuariosBindingSource;
    }
}
