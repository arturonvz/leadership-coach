USE [master]
GO
/****** Object:  Database [NurseSupDB]    Script Date: 23/11/2023 15:37:06 ******/
CREATE DATABASE [NurseSupDB]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'NurseSupDB', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\NurseSupDB.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'NurseSupDB_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\NurseSupDB_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [NurseSupDB] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [NurseSupDB].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [NurseSupDB] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [NurseSupDB] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [NurseSupDB] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [NurseSupDB] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [NurseSupDB] SET ARITHABORT OFF 
GO
ALTER DATABASE [NurseSupDB] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [NurseSupDB] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [NurseSupDB] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [NurseSupDB] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [NurseSupDB] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [NurseSupDB] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [NurseSupDB] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [NurseSupDB] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [NurseSupDB] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [NurseSupDB] SET  DISABLE_BROKER 
GO
ALTER DATABASE [NurseSupDB] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [NurseSupDB] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [NurseSupDB] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [NurseSupDB] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [NurseSupDB] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [NurseSupDB] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [NurseSupDB] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [NurseSupDB] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [NurseSupDB] SET  MULTI_USER 
GO
ALTER DATABASE [NurseSupDB] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [NurseSupDB] SET DB_CHAINING OFF 
GO
ALTER DATABASE [NurseSupDB] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [NurseSupDB] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [NurseSupDB] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [NurseSupDB] SET ACCELERATED_DATABASE_RECOVERY = OFF  
GO
ALTER DATABASE [NurseSupDB] SET QUERY_STORE = OFF
GO
USE [NurseSupDB]
GO
/****** Object:  Table [dbo].[Users]    Script Date: 23/11/2023 15:37:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Users](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[UserName] [nvarchar](30) NOT NULL,
	[FirstName] [nvarchar](50) NULL,
	[LastName] [nvarchar](50) NULL,
	[Department] [nvarchar](50) NULL,
	[Position] [nvarchar](30) NULL,
	[Password] [nvarchar](30) NULL,
	[HashedPassword] [binary](50) NULL,
	[idGrupo] [bigint] NULL,
	[Code] [nvarchar](20) NULL,
	[AppInfo] [nvarchar](500) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Groups]    Script Date: 23/11/2023 15:37:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Groups](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[GroupName] [nvarchar](50) NOT NULL,
	[PermissionsNum] [bigint] NULL
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[ViewUsers]    Script Date: 23/11/2023 15:37:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[ViewUsers]
AS
SELECT        dbo.Users.id, dbo.Users.UserName, dbo.Users.FirstName, dbo.Users.LastName, dbo.Users.Department, dbo.Users.Position, dbo.Users.Password, dbo.Users.HashedPassword, dbo.Users.idGrupo, dbo.Users.Code, 
                         dbo.Users.AppInfo, dbo.Groups.GroupName, dbo.Groups.PermissionsNum
FROM            dbo.Users INNER JOIN
                         dbo.Groups ON dbo.Users.idGrupo = dbo.Groups.id
GO
/****** Object:  Table [dbo].[Habilidades]    Script Date: 23/11/2023 15:37:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Habilidades](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[CódigoHabilidad] [nvarchar](15) NULL,
	[NombreHabilidad] [nvarchar](50) NULL,
	[CódigoCompetencia] [nvarchar](15) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Niveles]    Script Date: 23/11/2023 15:37:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Niveles](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[Nombre] [nvarchar](50) NULL,
	[Fase] [int] NULL,
	[Desbloqueado] [bit] NULL,
	[Dificultad] [int] NULL,
	[idHabilidad] [bigint] NULL,
	[DesbloquearWhere] [nvarchar](500) NULL,
	[DesbloquearId] [bigint] NULL,
	[Desbloqueos] [int] NULL,
	[DesbloqueosPerfecto] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[RegistroRondas]    Script Date: 23/11/2023 15:37:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RegistroRondas](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[FechaHoraInicio] [datetime] NULL,
	[FechaHoraFin] [datetime] NULL,
	[UserName] [nvarchar](30) NULL,
	[idHabilidad] [bigint] NULL,
	[idNivel] [bigint] NULL,
	[CantidadPreguntas] [int] NULL,
	[Correctas] [int] NULL,
	[Incorrectas] [int] NULL,
	[PuntajeExacto] [real] NULL,
	[Sorteo] [bit] NULL,
	[Estrellas] [int] NULL,
	[FueMejorPuntaje] [bit] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[RegistroRespuestas]    Script Date: 23/11/2023 15:37:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RegistroRespuestas](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[FechaHoraPregunta] [datetime] NULL,
	[FechaHoraRespuesta] [datetime] NULL,
	[UserName] [nvarchar](30) NULL,
	[Pregunta] [nvarchar](200) NULL,
	[RespuestaElegida] [nvarchar](100) NULL,
	[EsCorrecta] [bit] NULL,
	[idPregunta] [bigint] NULL,
	[idRonda] [bigint] NULL,
	[Dificultad] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[View_RegistroRespuestas]    Script Date: 23/11/2023 15:37:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[View_RegistroRespuestas]
AS
SELECT        dbo.RegistroRespuestas.id, dbo.RegistroRespuestas.FechaHoraPregunta, dbo.RegistroRespuestas.FechaHoraRespuesta, dbo.RegistroRespuestas.UserName, dbo.RegistroRespuestas.Pregunta, 
                         dbo.RegistroRespuestas.RespuestaElegida, dbo.RegistroRespuestas.EsCorrecta, dbo.RegistroRespuestas.idPregunta, dbo.RegistroRespuestas.idRonda, dbo.RegistroRespuestas.Dificultad, 
                         dbo.Habilidades.NombreHabilidad AS Habilidad, dbo.Niveles.Nombre AS Nivel, dbo.Users.FirstName, dbo.Users.LastName, dbo.Users.Department, dbo.Users.Position, dbo.Users.Code
FROM            dbo.RegistroRespuestas INNER JOIN
                         dbo.RegistroRondas ON dbo.RegistroRespuestas.idRonda = dbo.RegistroRondas.id INNER JOIN
                         dbo.Habilidades ON dbo.RegistroRondas.idHabilidad = dbo.Habilidades.id INNER JOIN
                         dbo.Niveles ON dbo.RegistroRondas.idNivel = dbo.Niveles.id INNER JOIN
                         dbo.Users ON dbo.RegistroRespuestas.UserName = dbo.Users.UserName
GO
/****** Object:  View [dbo].[View_RegistroRondas]    Script Date: 23/11/2023 15:37:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[View_RegistroRondas]
AS
SELECT        dbo.RegistroRondas.id, dbo.RegistroRondas.FechaHoraInicio, dbo.RegistroRondas.FechaHoraFin, dbo.RegistroRondas.UserName, dbo.RegistroRondas.idHabilidad, dbo.RegistroRondas.idNivel, 
                         dbo.RegistroRondas.CantidadPreguntas, dbo.RegistroRondas.Correctas, dbo.RegistroRondas.Incorrectas, dbo.RegistroRondas.PuntajeExacto, dbo.RegistroRondas.Sorteo, dbo.RegistroRondas.Estrellas, 
                         dbo.RegistroRondas.FueMejorPuntaje, dbo.Habilidades.NombreHabilidad AS Habilidad, dbo.Niveles.Nombre AS Nivel, dbo.Niveles.Dificultad, dbo.Niveles.DesbloquearWhere, dbo.Niveles.DesbloquearId, 
                         dbo.Niveles.Desbloqueos, dbo.Niveles.DesbloqueosPerfecto, dbo.Users.FirstName, dbo.Users.LastName, dbo.Users.Department, dbo.Users.Position, dbo.Users.Code
FROM            dbo.RegistroRondas INNER JOIN
                         dbo.Habilidades ON dbo.RegistroRondas.idHabilidad = dbo.Habilidades.id INNER JOIN
                         dbo.Niveles ON dbo.RegistroRondas.idNivel = dbo.Niveles.id INNER JOIN
                         dbo.Users ON dbo.RegistroRondas.UserName = dbo.Users.UserName
GO
/****** Object:  Table [dbo].[Competencias]    Script Date: 23/11/2023 15:37:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Competencias](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[CódigoCompetencia] [nvarchar](15) NULL,
	[NombreCompetencia] [nvarchar](50) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NivelesEscenas]    Script Date: 23/11/2023 15:37:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NivelesEscenas](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[Nombre] [nvarchar](50) NULL,
	[idNivel] [bigint] NULL,
	[Correlativo] [int] NULL,
	[TextoSuperior] [nvarchar](500) NULL,
	[TextoInferior] [nvarchar](500) NULL,
	[TextoMedio] [nvarchar](500) NULL,
	[ImagenFondoURL] [nvarchar](500) NULL,
	[RelativeURL] [bit] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NivelesEscenasFinales]    Script Date: 23/11/2023 15:37:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NivelesEscenasFinales](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[Nombre] [nvarchar](50) NULL,
	[idNivel] [bigint] NULL,
	[Correlativo] [int] NULL,
	[FinalBueno] [bit] NULL,
	[FinalMalo] [bit] NULL,
	[TextoSuperior] [nvarchar](500) NULL,
	[TextoInferior] [nvarchar](500) NULL,
	[TextoMedio] [nvarchar](500) NULL,
	[ImagenFondoURL] [nvarchar](500) NULL,
	[RelativeURL] [bit] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Permissions]    Script Date: 23/11/2023 15:37:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Permissions](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[PermissionName] [nvarchar](50) NOT NULL,
	[PermissionValue] [int] NOT NULL,
	[ControlName] [nvarchar](100) NOT NULL,
	[PropertyName] [nvarchar](50) NOT NULL,
	[IsNegated] [bit] NOT NULL,
 CONSTRAINT [PK_Permissions] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Preguntas]    Script Date: 23/11/2023 15:37:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Preguntas](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[idHabilidad] [bigint] NULL,
	[Dificultad] [int] NULL,
	[Pregunta] [nvarchar](200) NULL,
	[Respuesta] [nvarchar](100) NULL,
	[RespuestaFalsa1] [nvarchar](100) NULL,
	[RespuestaFalsa2] [nvarchar](100) NULL,
	[RespuestaFalsa3] [nvarchar](100) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Reglas]    Script Date: 23/11/2023 15:37:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Reglas](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[NombrePerfil] [nvarchar](50) NULL,
	[ProbabilidadMenor] [int] NULL,
	[ProbabilidadIgual] [int] NULL,
	[ProbabilidadMayor] [int] NULL,
	[PreguntasDificultad1] [int] NULL,
	[MultiplicadorPreguntas] [real] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UsuariosNiveles]    Script Date: 23/11/2023 15:37:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UsuariosNiveles](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[idUsuario] [bigint] NULL,
	[idNivel] [bigint] NULL,
	[Desbloqueado] [bit] NULL,
	[Estrellas] [int] NULL,
	[MejorPuntaje] [real] NULL
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Competencias] ON 

INSERT [dbo].[Competencias] ([id], [CódigoCompetencia], [NombreCompetencia]) VALUES (1, NULL, N'Liderazgo')
SET IDENTITY_INSERT [dbo].[Competencias] OFF
GO
SET IDENTITY_INSERT [dbo].[Groups] ON 

INSERT [dbo].[Groups] ([id], [GroupName], [PermissionsNum]) VALUES (1, N'Desarrollador', 9223372036854775807)
INSERT [dbo].[Groups] ([id], [GroupName], [PermissionsNum]) VALUES (2, N'Administrador', 9223372036854775807)
INSERT [dbo].[Groups] ([id], [GroupName], [PermissionsNum]) VALUES (3, N'Coach', 15)
INSERT [dbo].[Groups] ([id], [GroupName], [PermissionsNum]) VALUES (4, N'Player', 6)
SET IDENTITY_INSERT [dbo].[Groups] OFF
GO
SET IDENTITY_INSERT [dbo].[Habilidades] ON 

INSERT [dbo].[Habilidades] ([id], [CódigoHabilidad], [NombreHabilidad], [CódigoCompetencia]) VALUES (1, N'comunicación', N'Comunicación', N'Liderazgo')
INSERT [dbo].[Habilidades] ([id], [CódigoHabilidad], [NombreHabilidad], [CódigoCompetencia]) VALUES (2, N'trabajo-equipo', N'Trabajo en equipo', N'Liderazgo')
INSERT [dbo].[Habilidades] ([id], [CódigoHabilidad], [NombreHabilidad], [CódigoCompetencia]) VALUES (3, N'responsabilidad', N'Responsabilidad', N'Liderazgo')
INSERT [dbo].[Habilidades] ([id], [CódigoHabilidad], [NombreHabilidad], [CódigoCompetencia]) VALUES (4, N'empatía', N'Empatía', N'Liderazgo')
INSERT [dbo].[Habilidades] ([id], [CódigoHabilidad], [NombreHabilidad], [CódigoCompetencia]) VALUES (5, N'proactividad', N'Proactividad', N'Liderazgo')
INSERT [dbo].[Habilidades] ([id], [CódigoHabilidad], [NombreHabilidad], [CódigoCompetencia]) VALUES (6, N'compromiso', N'Compromiso', N'Liderazgo')
SET IDENTITY_INSERT [dbo].[Habilidades] OFF
GO
SET IDENTITY_INSERT [dbo].[Niveles] ON 

INSERT [dbo].[Niveles] ([id], [Nombre], [Fase], [Desbloqueado], [Dificultad], [idHabilidad], [DesbloquearWhere], [DesbloquearId], [Desbloqueos], [DesbloqueosPerfecto]) VALUES (1, N'Comenzando en el Hospital', 1, 1, 1, 1, N'(idHabilidad = 1 and Fase = 2) and (idHabilidad = 2 and Fase = 1) and Desbloqueado = 0', 0, 1, 2)
INSERT [dbo].[Niveles] ([id], [Nombre], [Fase], [Desbloqueado], [Dificultad], [idHabilidad], [DesbloquearWhere], [DesbloquearId], [Desbloqueos], [DesbloqueosPerfecto]) VALUES (2, N'Barreras y Expresión de Empatía', 2, 0, 2, 1, N'(idHabilidad = 1 and Fase = 3) and Desbloqueado = 0', 0, 1, 2)
INSERT [dbo].[Niveles] ([id], [Nombre], [Fase], [Desbloqueado], [Dificultad], [idHabilidad], [DesbloquearWhere], [DesbloquearId], [Desbloqueos], [DesbloqueosPerfecto]) VALUES (3, N'Adaptación y Colaboración', 3, 0, 3, 1, N'(idHabilidad > 2 and Fase = 1) and Desbloqueado = 0', 0, 1, 2)
INSERT [dbo].[Niveles] ([id], [Nombre], [Fase], [Desbloqueado], [Dificultad], [idHabilidad], [DesbloquearWhere], [DesbloquearId], [Desbloqueos], [DesbloqueosPerfecto]) VALUES (4, N'Comenzando en el Equipo Médico', 1, 0, 1, 2, N'(idHabilidad = 2 and Fase = 2) and Desbloqueado = 0', 0, 1, 1)
INSERT [dbo].[Niveles] ([id], [Nombre], [Fase], [Desbloqueado], [Dificultad], [idHabilidad], [DesbloquearWhere], [DesbloquearId], [Desbloqueos], [DesbloqueosPerfecto]) VALUES (5, N'Desafíos y Objetivos Comunes', 2, 0, 2, 2, N'(idHabilidad = 2 and Fase = 3) and Desbloqueado = 0', 0, 1, 1)
INSERT [dbo].[Niveles] ([id], [Nombre], [Fase], [Desbloqueado], [Dificultad], [idHabilidad], [DesbloquearWhere], [DesbloquearId], [Desbloqueos], [DesbloqueosPerfecto]) VALUES (6, N'Coordinación y Resultados Positivos', 3, 0, 3, 2, N'(idHabilidad > 2 and Fase = 1) and Desbloqueado = 0', 0, 1, 2)
INSERT [dbo].[Niveles] ([id], [Nombre], [Fase], [Desbloqueado], [Dificultad], [idHabilidad], [DesbloquearWhere], [DesbloquearId], [Desbloqueos], [DesbloqueosPerfecto]) VALUES (7, N'Responsabilidad: Historia 1', 1, 0, 1, 3, N'(idHabilidad = 3 and Fase = 2) and Desbloqueado = 0', 0, 1, 1)
INSERT [dbo].[Niveles] ([id], [Nombre], [Fase], [Desbloqueado], [Dificultad], [idHabilidad], [DesbloquearWhere], [DesbloquearId], [Desbloqueos], [DesbloqueosPerfecto]) VALUES (8, N'Responsabilidad: Historia 2', 2, 0, 2, 3, N'(idHabilidad = 3 and Fase = 3) and Desbloqueado = 0', 0, 1, 1)
INSERT [dbo].[Niveles] ([id], [Nombre], [Fase], [Desbloqueado], [Dificultad], [idHabilidad], [DesbloquearWhere], [DesbloquearId], [Desbloqueos], [DesbloqueosPerfecto]) VALUES (9, N'Responsabilidad: Historia 3', 3, 0, 3, 3, N'(idHabilidad > 2 and Fase = 1) and Desbloqueado = 0', 0, 1, 2)
INSERT [dbo].[Niveles] ([id], [Nombre], [Fase], [Desbloqueado], [Dificultad], [idHabilidad], [DesbloquearWhere], [DesbloquearId], [Desbloqueos], [DesbloqueosPerfecto]) VALUES (19, N'Empatía: Historia 1', 1, 0, 1, 4, N'(idHabilidad = 4 and Fase = 2) and Desbloqueado = 0', 0, 1, 1)
INSERT [dbo].[Niveles] ([id], [Nombre], [Fase], [Desbloqueado], [Dificultad], [idHabilidad], [DesbloquearWhere], [DesbloquearId], [Desbloqueos], [DesbloqueosPerfecto]) VALUES (20, N'Empatía: Historia 2', 2, 0, 2, 4, N'(idHabilidad = 4 and Fase = 3) and Desbloqueado = 0', 0, 1, 1)
INSERT [dbo].[Niveles] ([id], [Nombre], [Fase], [Desbloqueado], [Dificultad], [idHabilidad], [DesbloquearWhere], [DesbloquearId], [Desbloqueos], [DesbloqueosPerfecto]) VALUES (21, N'Empatía: Historia 3', 3, 0, 3, 4, N'(idHabilidad > 2 and Fase = 1) and Desbloqueado = 0', 0, 1, 2)
INSERT [dbo].[Niveles] ([id], [Nombre], [Fase], [Desbloqueado], [Dificultad], [idHabilidad], [DesbloquearWhere], [DesbloquearId], [Desbloqueos], [DesbloqueosPerfecto]) VALUES (22, N'Proactividad: Historia 1', 1, 0, 1, 5, N'(idHabilidad = 5 and Fase = 2) and Desbloqueado = 0', 0, 1, 1)
INSERT [dbo].[Niveles] ([id], [Nombre], [Fase], [Desbloqueado], [Dificultad], [idHabilidad], [DesbloquearWhere], [DesbloquearId], [Desbloqueos], [DesbloqueosPerfecto]) VALUES (23, N'Proactividad: Historia 2', 2, 0, 2, 5, N'(idHabilidad = 5 and Fase = 3) and Desbloqueado = 0', 0, 1, 1)
INSERT [dbo].[Niveles] ([id], [Nombre], [Fase], [Desbloqueado], [Dificultad], [idHabilidad], [DesbloquearWhere], [DesbloquearId], [Desbloqueos], [DesbloqueosPerfecto]) VALUES (24, N'Proactividad: Historia 3', 3, 0, 3, 5, N'(idHabilidad > 2 and Fase = 1) and Desbloqueado = 0', 0, 1, 2)
INSERT [dbo].[Niveles] ([id], [Nombre], [Fase], [Desbloqueado], [Dificultad], [idHabilidad], [DesbloquearWhere], [DesbloquearId], [Desbloqueos], [DesbloqueosPerfecto]) VALUES (25, N'Compromiso: Historia 1', 1, 0, 1, 6, N'(idHabilidad = 6 and Fase = 2) and Desbloqueado = 0', 0, 1, 1)
INSERT [dbo].[Niveles] ([id], [Nombre], [Fase], [Desbloqueado], [Dificultad], [idHabilidad], [DesbloquearWhere], [DesbloquearId], [Desbloqueos], [DesbloqueosPerfecto]) VALUES (26, N'Compromiso: Historia 2', 2, 0, 2, 6, N'(idHabilidad = 6 and Fase = 3) and Desbloqueado = 0', 0, 1, 1)
INSERT [dbo].[Niveles] ([id], [Nombre], [Fase], [Desbloqueado], [Dificultad], [idHabilidad], [DesbloquearWhere], [DesbloquearId], [Desbloqueos], [DesbloqueosPerfecto]) VALUES (27, N'Compromiso: Historia 3', 3, 0, 3, 6, N'(idHabilidad > 2 and Fase = 1) and Desbloqueado = 0', 0, 1, 2)
SET IDENTITY_INSERT [dbo].[Niveles] OFF
GO
SET IDENTITY_INSERT [dbo].[NivelesEscenas] ON 

INSERT [dbo].[NivelesEscenas] ([id], [Nombre], [idNivel], [Correlativo], [TextoSuperior], [TextoInferior], [TextoMedio], [ImagenFondoURL], [RelativeURL]) VALUES (1, N'NuevoEmpleado_01', 1, 1, N'Comenzando en el Hospital', N'Carlos, un nuevo empleado del Hospital Centro de Emergencia, estaba emocionado por su primer día. Su supervisora, Laura, lo saludó y lo llevó a la planta de enfermería. Carlos estaba ansioso por aprender y ser un gran miembro del equipo.', N'', N'', 1)
INSERT [dbo].[NivelesEscenas] ([id], [Nombre], [idNivel], [Correlativo], [TextoSuperior], [TextoInferior], [TextoMedio], [ImagenFondoURL], [RelativeURL]) VALUES (2, N'NuevoEmpleado_02', 1, 2, N'Comenzando en el Hospital', N'Carlos sugirió y preguntó: "¿Qué es la escucha activa?"', N'', N'', 1)
INSERT [dbo].[NivelesEscenas] ([id], [Nombre], [idNivel], [Correlativo], [TextoSuperior], [TextoInferior], [TextoMedio], [ImagenFondoURL], [RelativeURL]) VALUES (13, N'AdaptaciónColaboración_03', 3, 3, N'Adaptación y Colaboración', N'Finalmente, Carlos se unió a una reunión de comunicación interprofesional, donde diferentes miembros del equipo de salud trabajaron juntos para brindar una atención integral al paciente. Ahora Carlos necesitaba transmitir sus dudas de la comunicación interprofesional y sobre cómo manejar las situaciones de conflicto...', N'', N'', 1)
INSERT [dbo].[NivelesEscenas] ([id], [Nombre], [idNivel], [Correlativo], [TextoSuperior], [TextoInferior], [TextoMedio], [ImagenFondoURL], [RelativeURL]) VALUES (5, N'BarrerasEmpatía_01', 2, 1, N'Barreras y Expresión de Empatía', N'En su segundo día, Carlos experimentó una situación donde una paciente estaba ansiosa y necesitaba hablar sobre su condición. Sin embargo, ella estaba molesta por la falta de respuesta oportuna de un miembro del personal anterior.', N'', N'', 1)
INSERT [dbo].[NivelesEscenas] ([id], [Nombre], [idNivel], [Correlativo], [TextoSuperior], [TextoInferior], [TextoMedio], [ImagenFondoURL], [RelativeURL]) VALUES (6, N'BarrerasEmpatía_02', 2, 2, N'Barreras y Expresión de Empatía', N'Carlos se dio cuenta de que esto era una barrera para una comunicación efectiva. Recordó la lección de Laura y se esforzó en ser puntual y atento en sus futuras interacciones.', N'', N'', 1)
INSERT [dbo].[NivelesEscenas] ([id], [Nombre], [idNivel], [Correlativo], [TextoSuperior], [TextoInferior], [TextoMedio], [ImagenFondoURL], [RelativeURL]) VALUES (8, N'AdaptaciónColaboración_01', 3, 1, N'Adaptación y Colaboración', N'Después de unas semanas, Carlos se sintió más cómodo en su papel. Aprendí que la comunicación efectiva implicaba adaptar su estilo para satisfacer las necesidades del público objetivo. Diferentes pacientes requerían diferentes enfoques para comprender mejor el mensaje.
', N'', N'', 1)
INSERT [dbo].[NivelesEscenas] ([id], [Nombre], [idNivel], [Correlativo], [TextoSuperior], [TextoInferior], [TextoMedio], [ImagenFondoURL], [RelativeURL]) VALUES (9, N'AdaptaciónColaboración_02', 3, 2, N'Adaptación y Colaboración', N'Un día, durante el cambio de turno, Carlos se dio cuenta de que un informe completo y preciso era fundamental para la seguridad del paciente. Él comprendió que el propósito de un informe de cambio de turno era asegurar que la información crítica se compartiera adecuadamente entre los turnos.
', N'', N'', 1)
SET IDENTITY_INSERT [dbo].[NivelesEscenas] OFF
GO
SET IDENTITY_INSERT [dbo].[NivelesEscenasFinales] ON 

INSERT [dbo].[NivelesEscenasFinales] ([id], [Nombre], [idNivel], [Correlativo], [FinalBueno], [FinalMalo], [TextoSuperior], [TextoInferior], [TextoMedio], [ImagenFondoURL], [RelativeURL]) VALUES (6, N'NuevoEmpleadoFinBueno_01', 1, 1, 1, 0, N'Comenzando en el Hospital', N'Laura notó su entusiasmo y le explicó: "Carlos, una de las competencias clave para trabajar aquí es la comunicación efectiva. Esto significa escuchar atentamente a nuestros pacientes y colegas".', N'', N'', 1)
INSERT [dbo].[NivelesEscenasFinales] ([id], [Nombre], [idNivel], [Correlativo], [FinalBueno], [FinalMalo], [TextoSuperior], [TextoInferior], [TextoMedio], [ImagenFondoURL], [RelativeURL]) VALUES (7, N'NuevoEmpleadoFinMalo_01', 1, 1, 0, 1, N'Comenzando en el Hospital', N'Laura molesta y le explicó: "La escucha activa es prestar total atención a la persona que está hablando, mostrando interés y haciendo preguntas para comprender mejor su mensaje. Es una parte importante de nuestra comunicación aquí".', N'', N'', 1)
INSERT [dbo].[NivelesEscenasFinales] ([id], [Nombre], [idNivel], [Correlativo], [FinalBueno], [FinalMalo], [TextoSuperior], [TextoInferior], [TextoMedio], [ImagenFondoURL], [RelativeURL]) VALUES (8, N'BarrerasEmpatíaFinBueno_01', 2, 1, 1, 0, N'Barreras y Expresión de Empatía', N'Más adelante, Carlos habló con Laura sobre la comunicación no verbal y cómo podía mejorarla. Laura le aconsejó practicar el lenguaje corporal abierto y mantener contacto visual, lo que ayudaría a transmitir empatía.', N'', N'', 1)
INSERT [dbo].[NivelesEscenasFinales] ([id], [Nombre], [idNivel], [Correlativo], [FinalBueno], [FinalMalo], [TextoSuperior], [TextoInferior], [TextoMedio], [ImagenFondoURL], [RelativeURL]) VALUES (9, N'BarrerasEmpatíaFinMalo_01', 2, 1, 0, 1, N'Barreras y Expresión de Empatía', N'Carlos no habló con Laura sobre la comunicación no verbal y cómo podía mejorarla. Esto le llevó a problemas para transmitir empatía y se estancó en este punto durante algún tiempo hasta que volvió a hablar con Laura.', N'', N'', 1)
INSERT [dbo].[NivelesEscenasFinales] ([id], [Nombre], [idNivel], [Correlativo], [FinalBueno], [FinalMalo], [TextoSuperior], [TextoInferior], [TextoMedio], [ImagenFondoURL], [RelativeURL]) VALUES (17, N'AdaptaciónColaboraciónFinBueno_01', 3, 1, 0, 0, N'Adaptación y Colaboración', N'Carlos se sintió orgulloso de ser parte de un equipo comprometido en brindar una atención de calidad.Carlos también aprendió cómo manejar la comunicación en situaciones de conflicto.', N'', N'', 1)
INSERT [dbo].[NivelesEscenasFinales] ([id], [Nombre], [idNivel], [Correlativo], [FinalBueno], [FinalMalo], [TextoSuperior], [TextoInferior], [TextoMedio], [ImagenFondoURL], [RelativeURL]) VALUES (18, N'AdaptaciónColaboraciónFinBueno_02', 3, 2, 1, 0, N'Adaptación y Colaboración', N'Carlos también aprendió cómo manejar la comunicación en situaciones de conflicto. Escuchar activamente, preocupaciones identificar y buscar soluciones que beneficien a ambas partes se convirtieron en herramientas importantes en su caja de habilidades.', N'', N'', 1)
INSERT [dbo].[NivelesEscenasFinales] ([id], [Nombre], [idNivel], [Correlativo], [FinalBueno], [FinalMalo], [TextoSuperior], [TextoInferior], [TextoMedio], [ImagenFondoURL], [RelativeURL]) VALUES (19, N'AdaptaciónColaboraciónFinBueno_03', 3, 3, 1, 0, N'Adaptación y Colaboración', N'Con el tiempo, Carlos se convirtió en un miembro valioso del equipo de enfermería, y su comunicación efectiva contribuyó a una mayor seguridad del paciente y satisfacción en el hospital.', N'', N'', 1)
INSERT [dbo].[NivelesEscenasFinales] ([id], [Nombre], [idNivel], [Correlativo], [FinalBueno], [FinalMalo], [TextoSuperior], [TextoInferior], [TextoMedio], [ImagenFondoURL], [RelativeURL]) VALUES (20, N'AdaptaciónColaboraciónFinMalo_01', 3, 1, 0, 1, N'Adaptación y Colaboración', N'Carlos no transmitió sus dudas y tuvo que esperar a la siguiente reunión para poder tratar con el equipo interprofesional completo para poder volver a intentarlo.', N'', N'', 1)
SET IDENTITY_INSERT [dbo].[NivelesEscenasFinales] OFF
GO
SET IDENTITY_INSERT [dbo].[Permissions] ON 

INSERT [dbo].[Permissions] ([id], [PermissionName], [PermissionValue], [ControlName], [PropertyName], [IsNegated]) VALUES (3, N'Game', 0, N'btnGame', N'Enabled', 0)
INSERT [dbo].[Permissions] ([id], [PermissionName], [PermissionValue], [ControlName], [PropertyName], [IsNegated]) VALUES (4, N'Historial', 1, N'btnHistorial', N'Enabled', 0)
INSERT [dbo].[Permissions] ([id], [PermissionName], [PermissionValue], [ControlName], [PropertyName], [IsNegated]) VALUES (5, N'Configuración', 2, N'btnConfiguración', N'Enabled', 0)
SET IDENTITY_INSERT [dbo].[Permissions] OFF
GO
SET IDENTITY_INSERT [dbo].[Preguntas] ON 

INSERT [dbo].[Preguntas] ([id], [idHabilidad], [Dificultad], [Pregunta], [Respuesta], [RespuestaFalsa1], [RespuestaFalsa2], [RespuestaFalsa3]) VALUES (2, 1, 1, N'¿Cuál de las siguientes opciones es una barrera común para una comunicación efectiva? ', N'Falta de respuesta adecuada. ', N'Hablar claramente y con calma. ', N'Escuchar activamente. ', N'Uso de lenguaje claro. ')
INSERT [dbo].[Preguntas] ([id], [idHabilidad], [Dificultad], [Pregunta], [Respuesta], [RespuestaFalsa1], [RespuestaFalsa2], [RespuestaFalsa3]) VALUES (3, 1, 3, N'¿Cómo se puede mejorar la comunicación no verbal?', N'Practicar el lenaguaje corporal abierto y mantener un contacto visual adecuado.', N'Hablar con mayor volumen. ', N'Evitar el contacto visual. ', N'Utilizar un lenguaje coporal cerrado. ')
INSERT [dbo].[Preguntas] ([id], [idHabilidad], [Dificultad], [Pregunta], [Respuesta], [RespuestaFalsa1], [RespuestaFalsa2], [RespuestaFalsa3]) VALUES (5, 1, 2, N'¿Cuál es un ejemplo de una pregunta abierta?', N'¿Puedes hablarme más sobre su experiencia?', N'¿Te gusto el evento?', N'¿La comida era buena?', N'¿Tuviste una buena experiencia?')
INSERT [dbo].[Preguntas] ([id], [idHabilidad], [Dificultad], [Pregunta], [Respuesta], [RespuestaFalsa1], [RespuestaFalsa2], [RespuestaFalsa3]) VALUES (6, 1, 2, N'¿Cómo puedes expresar empaía en una conversación?', N'Mostrando compresión y apoyo emocional hacia las preocupaciones del interlocutor. ', N'Cambiar de tema constantemente ', N'Ignorar las emociones del interlocutor. ', N'Dar soluciones inmediatas. ')
INSERT [dbo].[Preguntas] ([id], [idHabilidad], [Dificultad], [Pregunta], [Respuesta], [RespuestaFalsa1], [RespuestaFalsa2], [RespuestaFalsa3]) VALUES (7, 1, 3, N'¿Cuál es un beneficio de atención de la comunicación efectiva en el entorno de médica? ', N'Mayor seguridad del paciente y satisfacción. ', N'Menos documentatación ', N'Menos tiempo empleado en la comunicación', N'Menos formación requerida ')
INSERT [dbo].[Preguntas] ([id], [idHabilidad], [Dificultad], [Pregunta], [Respuesta], [RespuestaFalsa1], [RespuestaFalsa2], [RespuestaFalsa3]) VALUES (8, 1, 3, N'¿Qué es la comunicación interprofesional?', N'La colaboración entre miembros de diferentes disciplinas para brindar atención integral al paciente.', N'La comunicación solo entre profesionales de la misma disciplina. ', N'La comunicación entre pacientes y profesionales de la salud. ', N'La comunicación con amigos y familiares en el hospital. ')
INSERT [dbo].[Preguntas] ([id], [idHabilidad], [Dificultad], [Pregunta], [Respuesta], [RespuestaFalsa1], [RespuestaFalsa2], [RespuestaFalsa3]) VALUES (9, 1, 1, N'¿Cuál es el proporsito de un informe de cambio de turbo?', N'Asegurar que la información crítica sobre el paciente se comparte adecuadamente entre los turnos. ', N'Proporcionar información detallada sobre el hospital.', N'Mantener registros de seguimiento. ', N'Discutir temas no relacionados con el trabajo. ')
INSERT [dbo].[Preguntas] ([id], [idHabilidad], [Dificultad], [Pregunta], [Respuesta], [RespuestaFalsa1], [RespuestaFalsa2], [RespuestaFalsa3]) VALUES (10, 1, 1, N'¿Cómo se puede manejar la comunicación en situaciones de conflicto?', N'Escuchar activamente, identificar preocupaciones y buscar soluciones que beneficien a ambas partes. ', N'Evitar el conflicto.', N'Ignorar las preocupaciones de la otra parte. ', N'Gritar y enfadarse. ')
INSERT [dbo].[Preguntas] ([id], [idHabilidad], [Dificultad], [Pregunta], [Respuesta], [RespuestaFalsa1], [RespuestaFalsa2], [RespuestaFalsa3]) VALUES (11, 1, 1, N'Cuál opción consiste en prestar total atención a la persona que esta hablando, mostrando interés y haciendo preguntas para comprender mejor su mensaje.', N'Escucha activa.', N'Opción 2', N'Opción 3', N'Opción 4')
INSERT [dbo].[Preguntas] ([id], [idHabilidad], [Dificultad], [Pregunta], [Respuesta], [RespuestaFalsa1], [RespuestaFalsa2], [RespuestaFalsa3]) VALUES (12, 2, 1, N'¿Cuál es la importancia del trabajo en equipo en el entorno de la salud? ', N'Mejora la calidad de la atención al paciente. ', N'Incrementar los costos de atención médica. ', N'Reducir la calidad de la atención al paciente. ', N'Minimizar la necesudad de colaboración. ')
INSERT [dbo].[Preguntas] ([id], [idHabilidad], [Dificultad], [Pregunta], [Respuesta], [RespuestaFalsa1], [RespuestaFalsa2], [RespuestaFalsa3]) VALUES (13, 2, 1, N'¿Qué caratceriza a un buen colaborador en un equipo de atención médica? ', N'Ser confiable, respetuoso y comunicarse de manera efectiva. ', N'Ser autocratico y dominante', N'Ser indiferente y poco comunicativo.', N'Ser competitivo y desconfiado. ')
INSERT [dbo].[Preguntas] ([id], [idHabilidad], [Dificultad], [Pregunta], [Respuesta], [RespuestaFalsa1], [RespuestaFalsa2], [RespuestaFalsa3]) VALUES (14, 2, 1, N'¿Qué es un objetivo común en un equipo de atención médica? ', N'Brindar atención segura y de alta calidad a los pacientes. ', N'Maximizar los costos de atención al paciente. ', N'Brindar atención de baja calidad a los pacientes. ', N'Limitar la comunicación entre los miembros del equipo. ')
INSERT [dbo].[Preguntas] ([id], [idHabilidad], [Dificultad], [Pregunta], [Respuesta], [RespuestaFalsa1], [RespuestaFalsa2], [RespuestaFalsa3]) VALUES (15, 2, 2, N'¿Cuál es un ejemplo de un desafío común en el trabajo en equipo en un hospital? ', N'Falta de comunicación efectiva entre los miembros del equipo. ', N'Exceso de comunicación efectiva. ', N'Mútiples reuniones de equipo. ', N'Demasiada retroalimentación constructiva. ')
INSERT [dbo].[Preguntas] ([id], [idHabilidad], [Dificultad], [Pregunta], [Respuesta], [RespuestaFalsa1], [RespuestaFalsa2], [RespuestaFalsa3]) VALUES (22, 3, 1, N'Habilidad Blanda: Responsabilidad Pregunta 1', N'Respuesta correcta.', N'Opción incorrecta 1', N'Opción incorrecta 2', N'Opción incorrecta 3')
INSERT [dbo].[Preguntas] ([id], [idHabilidad], [Dificultad], [Pregunta], [Respuesta], [RespuestaFalsa1], [RespuestaFalsa2], [RespuestaFalsa3]) VALUES (23, 3, 1, N'Habilidad Blanda: Responsabilidad Pregunta 2', N'Respuesta correcta.', N'Opción incorrecta 1', N'Opción incorrecta 2', N'Opción incorrecta 3')
INSERT [dbo].[Preguntas] ([id], [idHabilidad], [Dificultad], [Pregunta], [Respuesta], [RespuestaFalsa1], [RespuestaFalsa2], [RespuestaFalsa3]) VALUES (24, 3, 1, N'Habilidad Blanda: Responsabilidad Pregunta 3', N'Respuesta correcta.', N'Opción incorrecta 1', N'Opción incorrecta 2', N'Opción incorrecta 3')
INSERT [dbo].[Preguntas] ([id], [idHabilidad], [Dificultad], [Pregunta], [Respuesta], [RespuestaFalsa1], [RespuestaFalsa2], [RespuestaFalsa3]) VALUES (16, 2, 2, N'¿Cómo se puede fomentar un entorno de trabajo en equipo positivo? ', N'Promoviendo la colaboración, la retroalimentación constructiva y el respeto mutuo. ', N'Minimizando la colaboración. ', N'Ignorando las preocupaciones de los demás. ', N'Limintando el respeto mutuo. ')
INSERT [dbo].[Preguntas] ([id], [idHabilidad], [Dificultad], [Pregunta], [Respuesta], [RespuestaFalsa1], [RespuestaFalsa2], [RespuestaFalsa3]) VALUES (17, 2, 2, N'¿Cuál es el propósito de las rondas multidisciplinarias? ', N'Coordinar la atención y discutir el plan de tratamiento del paciente entre diferentes profesionales.', N'Reducir la coordinación de la atención al paciente.', N'Minimizar la comunicación entre profesionales. ', N'Limitar el acceso a los registros médicos. ')
INSERT [dbo].[Preguntas] ([id], [idHabilidad], [Dificultad], [Pregunta], [Respuesta], [RespuestaFalsa1], [RespuestaFalsa2], [RespuestaFalsa3]) VALUES (18, 2, 3, N'¿Cómo puedes contribuir al trabajo en equipo en una situación de emergencia? ', N'Seguir los roles asignados, comunicar claramente y prestar asistencia cuando sea necsario. ', N'Ignorar los roles asignados. ', N'Comunicar de manera ambigua. ', N'Evitar prestar asistencia. ')
INSERT [dbo].[Preguntas] ([id], [idHabilidad], [Dificultad], [Pregunta], [Respuesta], [RespuestaFalsa1], [RespuestaFalsa2], [RespuestaFalsa3]) VALUES (19, 2, 3, N'¿Cuál es un ejemplo de una actividad que promueve el trabajo en equipo?', N'Realizar una revisión de casos entre profesionales de diferentes especialidades. ', N'Evitar la colaboración.', N'Fomentar la competencia entre los miembros del equipo. ', N'Limitar la interacción entre profesionales.')
INSERT [dbo].[Preguntas] ([id], [idHabilidad], [Dificultad], [Pregunta], [Respuesta], [RespuestaFalsa1], [RespuestaFalsa2], [RespuestaFalsa3]) VALUES (20, 2, 1, N'¿Cómo se puede resolver un conflicto dentro de un equipo de atención médica?', N'Escuchar las preocupaciones de todos, buscar soluciones consensuadas y seguir adelante. ', N'Ignorar las preocupaciones de todos.', N'Evitar buscar soluciones ', N'Incrementar el conflicto. ')
INSERT [dbo].[Preguntas] ([id], [idHabilidad], [Dificultad], [Pregunta], [Respuesta], [RespuestaFalsa1], [RespuestaFalsa2], [RespuestaFalsa3]) VALUES (21, 2, 1, N'¿Qué es un resultado positivo del trabajo en equipo en el entorno de la salud?', N'Mayor eficiencia en la atención al paciente y una reducción de errores. ', N'Aumentar la ineficiencia en la atención al apciente. ', N'Incrementar el número de errores en la atención al paciente. ', N'Reducir la calidad de la atención al paciente. ')
INSERT [dbo].[Preguntas] ([id], [idHabilidad], [Dificultad], [Pregunta], [Respuesta], [RespuestaFalsa1], [RespuestaFalsa2], [RespuestaFalsa3]) VALUES (25, 3, 1, N'Habilidad Blanda: Responsabilidad Pregunta 4', N'Respuesta correcta.', N'Opción incorrecta 1', N'Opción incorrecta 2', N'Opción incorrecta 3')
INSERT [dbo].[Preguntas] ([id], [idHabilidad], [Dificultad], [Pregunta], [Respuesta], [RespuestaFalsa1], [RespuestaFalsa2], [RespuestaFalsa3]) VALUES (26, 3, 2, N'Habilidad Blanda: Responsabilidad Pregunta 5', N'Respuesta correcta.', N'Opción incorrecta 1', N'Opción incorrecta 2', N'Opción incorrecta 3')
INSERT [dbo].[Preguntas] ([id], [idHabilidad], [Dificultad], [Pregunta], [Respuesta], [RespuestaFalsa1], [RespuestaFalsa2], [RespuestaFalsa3]) VALUES (27, 3, 2, N'Habilidad Blanda: Responsabilidad Pregunta 6', N'Respuesta correcta.', N'Opción incorrecta 1', N'Opción incorrecta 2', N'Opción incorrecta 3')
INSERT [dbo].[Preguntas] ([id], [idHabilidad], [Dificultad], [Pregunta], [Respuesta], [RespuestaFalsa1], [RespuestaFalsa2], [RespuestaFalsa3]) VALUES (28, 3, 2, N'Habilidad Blanda: Responsabilidad Pregunta 7', N'Respuesta correcta.', N'Opción incorrecta 1', N'Opción incorrecta 2', N'Opción incorrecta 3')
INSERT [dbo].[Preguntas] ([id], [idHabilidad], [Dificultad], [Pregunta], [Respuesta], [RespuestaFalsa1], [RespuestaFalsa2], [RespuestaFalsa3]) VALUES (29, 3, 3, N'Habilidad Blanda: Responsabilidad Pregunta 8', N'Respuesta correcta.', N'Opción incorrecta 1', N'Opción incorrecta 2', N'Opción incorrecta 3')
INSERT [dbo].[Preguntas] ([id], [idHabilidad], [Dificultad], [Pregunta], [Respuesta], [RespuestaFalsa1], [RespuestaFalsa2], [RespuestaFalsa3]) VALUES (30, 3, 3, N'Habilidad Blanda: Responsabilidad Pregunta 9', N'Respuesta correcta.', N'Opción incorrecta 1', N'Opción incorrecta 2', N'Opción incorrecta 3')
INSERT [dbo].[Preguntas] ([id], [idHabilidad], [Dificultad], [Pregunta], [Respuesta], [RespuestaFalsa1], [RespuestaFalsa2], [RespuestaFalsa3]) VALUES (31, 3, 3, N'Habilidad Blanda: Responsabilidad Pregunta 10', N'Respuesta correcta.', N'Opción incorrecta 1', N'Opción incorrecta 2', N'Opción incorrecta 3')
INSERT [dbo].[Preguntas] ([id], [idHabilidad], [Dificultad], [Pregunta], [Respuesta], [RespuestaFalsa1], [RespuestaFalsa2], [RespuestaFalsa3]) VALUES (32, 4, 1, N'Habilidad Blanda: Responsabilidad Pregunta 1', N'Respuesta correcta.', N'Opción incorrecta 1', N'Opción incorrecta 2', N'Opción incorrecta 3')
INSERT [dbo].[Preguntas] ([id], [idHabilidad], [Dificultad], [Pregunta], [Respuesta], [RespuestaFalsa1], [RespuestaFalsa2], [RespuestaFalsa3]) VALUES (33, 4, 1, N'Habilidad Blanda: Responsabilidad Pregunta 2', N'Respuesta correcta.', N'Opción incorrecta 1', N'Opción incorrecta 2', N'Opción incorrecta 3')
INSERT [dbo].[Preguntas] ([id], [idHabilidad], [Dificultad], [Pregunta], [Respuesta], [RespuestaFalsa1], [RespuestaFalsa2], [RespuestaFalsa3]) VALUES (34, 4, 1, N'Habilidad Blanda: Responsabilidad Pregunta 3', N'Respuesta correcta.', N'Opción incorrecta 1', N'Opción incorrecta 2', N'Opción incorrecta 3')
INSERT [dbo].[Preguntas] ([id], [idHabilidad], [Dificultad], [Pregunta], [Respuesta], [RespuestaFalsa1], [RespuestaFalsa2], [RespuestaFalsa3]) VALUES (35, 4, 1, N'Habilidad Blanda: Responsabilidad Pregunta 4', N'Respuesta correcta.', N'Opción incorrecta 1', N'Opción incorrecta 2', N'Opción incorrecta 3')
INSERT [dbo].[Preguntas] ([id], [idHabilidad], [Dificultad], [Pregunta], [Respuesta], [RespuestaFalsa1], [RespuestaFalsa2], [RespuestaFalsa3]) VALUES (36, 4, 2, N'Habilidad Blanda: Responsabilidad Pregunta 5', N'Respuesta correcta.', N'Opción incorrecta 1', N'Opción incorrecta 2', N'Opción incorrecta 3')
INSERT [dbo].[Preguntas] ([id], [idHabilidad], [Dificultad], [Pregunta], [Respuesta], [RespuestaFalsa1], [RespuestaFalsa2], [RespuestaFalsa3]) VALUES (37, 4, 2, N'Habilidad Blanda: Responsabilidad Pregunta 6', N'Respuesta correcta.', N'Opción incorrecta 1', N'Opción incorrecta 2', N'Opción incorrecta 3')
INSERT [dbo].[Preguntas] ([id], [idHabilidad], [Dificultad], [Pregunta], [Respuesta], [RespuestaFalsa1], [RespuestaFalsa2], [RespuestaFalsa3]) VALUES (38, 4, 2, N'Habilidad Blanda: Responsabilidad Pregunta 7', N'Respuesta correcta.', N'Opción incorrecta 1', N'Opción incorrecta 2', N'Opción incorrecta 3')
INSERT [dbo].[Preguntas] ([id], [idHabilidad], [Dificultad], [Pregunta], [Respuesta], [RespuestaFalsa1], [RespuestaFalsa2], [RespuestaFalsa3]) VALUES (39, 4, 3, N'Habilidad Blanda: Responsabilidad Pregunta 8', N'Respuesta correcta.', N'Opción incorrecta 1', N'Opción incorrecta 2', N'Opción incorrecta 3')
INSERT [dbo].[Preguntas] ([id], [idHabilidad], [Dificultad], [Pregunta], [Respuesta], [RespuestaFalsa1], [RespuestaFalsa2], [RespuestaFalsa3]) VALUES (40, 4, 3, N'Habilidad Blanda: Responsabilidad Pregunta 9', N'Respuesta correcta.', N'Opción incorrecta 1', N'Opción incorrecta 2', N'Opción incorrecta 3')
INSERT [dbo].[Preguntas] ([id], [idHabilidad], [Dificultad], [Pregunta], [Respuesta], [RespuestaFalsa1], [RespuestaFalsa2], [RespuestaFalsa3]) VALUES (41, 4, 3, N'Habilidad Blanda: Responsabilidad Pregunta 10', N'Respuesta correcta.', N'Opción incorrecta 1', N'Opción incorrecta 2', N'Opción incorrecta 3')
INSERT [dbo].[Preguntas] ([id], [idHabilidad], [Dificultad], [Pregunta], [Respuesta], [RespuestaFalsa1], [RespuestaFalsa2], [RespuestaFalsa3]) VALUES (42, 5, 1, N'Habilidad Blanda: Responsabilidad Pregunta 1', N'Respuesta correcta.', N'Opción incorrecta 1', N'Opción incorrecta 2', N'Opción incorrecta 3')
INSERT [dbo].[Preguntas] ([id], [idHabilidad], [Dificultad], [Pregunta], [Respuesta], [RespuestaFalsa1], [RespuestaFalsa2], [RespuestaFalsa3]) VALUES (43, 5, 1, N'Habilidad Blanda: Responsabilidad Pregunta 2', N'Respuesta correcta.', N'Opción incorrecta 1', N'Opción incorrecta 2', N'Opción incorrecta 3')
INSERT [dbo].[Preguntas] ([id], [idHabilidad], [Dificultad], [Pregunta], [Respuesta], [RespuestaFalsa1], [RespuestaFalsa2], [RespuestaFalsa3]) VALUES (44, 5, 1, N'Habilidad Blanda: Responsabilidad Pregunta 3', N'Respuesta correcta.', N'Opción incorrecta 1', N'Opción incorrecta 2', N'Opción incorrecta 3')
INSERT [dbo].[Preguntas] ([id], [idHabilidad], [Dificultad], [Pregunta], [Respuesta], [RespuestaFalsa1], [RespuestaFalsa2], [RespuestaFalsa3]) VALUES (45, 5, 1, N'Habilidad Blanda: Responsabilidad Pregunta 4', N'Respuesta correcta.', N'Opción incorrecta 1', N'Opción incorrecta 2', N'Opción incorrecta 3')
INSERT [dbo].[Preguntas] ([id], [idHabilidad], [Dificultad], [Pregunta], [Respuesta], [RespuestaFalsa1], [RespuestaFalsa2], [RespuestaFalsa3]) VALUES (46, 5, 2, N'Habilidad Blanda: Responsabilidad Pregunta 5', N'Respuesta correcta.', N'Opción incorrecta 1', N'Opción incorrecta 2', N'Opción incorrecta 3')
INSERT [dbo].[Preguntas] ([id], [idHabilidad], [Dificultad], [Pregunta], [Respuesta], [RespuestaFalsa1], [RespuestaFalsa2], [RespuestaFalsa3]) VALUES (47, 5, 2, N'Habilidad Blanda: Responsabilidad Pregunta 6', N'Respuesta correcta.', N'Opción incorrecta 1', N'Opción incorrecta 2', N'Opción incorrecta 3')
INSERT [dbo].[Preguntas] ([id], [idHabilidad], [Dificultad], [Pregunta], [Respuesta], [RespuestaFalsa1], [RespuestaFalsa2], [RespuestaFalsa3]) VALUES (48, 5, 2, N'Habilidad Blanda: Responsabilidad Pregunta 7', N'Respuesta correcta.', N'Opción incorrecta 1', N'Opción incorrecta 2', N'Opción incorrecta 3')
INSERT [dbo].[Preguntas] ([id], [idHabilidad], [Dificultad], [Pregunta], [Respuesta], [RespuestaFalsa1], [RespuestaFalsa2], [RespuestaFalsa3]) VALUES (49, 5, 3, N'Habilidad Blanda: Responsabilidad Pregunta 8', N'Respuesta correcta.', N'Opción incorrecta 1', N'Opción incorrecta 2', N'Opción incorrecta 3')
INSERT [dbo].[Preguntas] ([id], [idHabilidad], [Dificultad], [Pregunta], [Respuesta], [RespuestaFalsa1], [RespuestaFalsa2], [RespuestaFalsa3]) VALUES (50, 5, 3, N'Habilidad Blanda: Responsabilidad Pregunta 9', N'Respuesta correcta.', N'Opción incorrecta 1', N'Opción incorrecta 2', N'Opción incorrecta 3')
INSERT [dbo].[Preguntas] ([id], [idHabilidad], [Dificultad], [Pregunta], [Respuesta], [RespuestaFalsa1], [RespuestaFalsa2], [RespuestaFalsa3]) VALUES (51, 5, 3, N'Habilidad Blanda: Responsabilidad Pregunta 10', N'Respuesta correcta.', N'Opción incorrecta 1', N'Opción incorrecta 2', N'Opción incorrecta 3')
INSERT [dbo].[Preguntas] ([id], [idHabilidad], [Dificultad], [Pregunta], [Respuesta], [RespuestaFalsa1], [RespuestaFalsa2], [RespuestaFalsa3]) VALUES (52, 6, 1, N'Habilidad Blanda: Responsabilidad Pregunta 1', N'Respuesta correcta.', N'Opción incorrecta 1', N'Opción incorrecta 2', N'Opción incorrecta 3')
INSERT [dbo].[Preguntas] ([id], [idHabilidad], [Dificultad], [Pregunta], [Respuesta], [RespuestaFalsa1], [RespuestaFalsa2], [RespuestaFalsa3]) VALUES (53, 6, 1, N'Habilidad Blanda: Responsabilidad Pregunta 2', N'Respuesta correcta.', N'Opción incorrecta 1', N'Opción incorrecta 2', N'Opción incorrecta 3')
INSERT [dbo].[Preguntas] ([id], [idHabilidad], [Dificultad], [Pregunta], [Respuesta], [RespuestaFalsa1], [RespuestaFalsa2], [RespuestaFalsa3]) VALUES (54, 6, 1, N'Habilidad Blanda: Responsabilidad Pregunta 3', N'Respuesta correcta.', N'Opción incorrecta 1', N'Opción incorrecta 2', N'Opción incorrecta 3')
INSERT [dbo].[Preguntas] ([id], [idHabilidad], [Dificultad], [Pregunta], [Respuesta], [RespuestaFalsa1], [RespuestaFalsa2], [RespuestaFalsa3]) VALUES (55, 6, 1, N'Habilidad Blanda: Responsabilidad Pregunta 4', N'Respuesta correcta.', N'Opción incorrecta 1', N'Opción incorrecta 2', N'Opción incorrecta 3')
INSERT [dbo].[Preguntas] ([id], [idHabilidad], [Dificultad], [Pregunta], [Respuesta], [RespuestaFalsa1], [RespuestaFalsa2], [RespuestaFalsa3]) VALUES (56, 6, 2, N'Habilidad Blanda: Responsabilidad Pregunta 5', N'Respuesta correcta.', N'Opción incorrecta 1', N'Opción incorrecta 2', N'Opción incorrecta 3')
INSERT [dbo].[Preguntas] ([id], [idHabilidad], [Dificultad], [Pregunta], [Respuesta], [RespuestaFalsa1], [RespuestaFalsa2], [RespuestaFalsa3]) VALUES (57, 6, 2, N'Habilidad Blanda: Responsabilidad Pregunta 6', N'Respuesta correcta.', N'Opción incorrecta 1', N'Opción incorrecta 2', N'Opción incorrecta 3')
INSERT [dbo].[Preguntas] ([id], [idHabilidad], [Dificultad], [Pregunta], [Respuesta], [RespuestaFalsa1], [RespuestaFalsa2], [RespuestaFalsa3]) VALUES (58, 6, 2, N'Habilidad Blanda: Responsabilidad Pregunta 7', N'Respuesta correcta.', N'Opción incorrecta 1', N'Opción incorrecta 2', N'Opción incorrecta 3')
INSERT [dbo].[Preguntas] ([id], [idHabilidad], [Dificultad], [Pregunta], [Respuesta], [RespuestaFalsa1], [RespuestaFalsa2], [RespuestaFalsa3]) VALUES (59, 6, 3, N'Habilidad Blanda: Responsabilidad Pregunta 8', N'Respuesta correcta.', N'Opción incorrecta 1', N'Opción incorrecta 2', N'Opción incorrecta 3')
INSERT [dbo].[Preguntas] ([id], [idHabilidad], [Dificultad], [Pregunta], [Respuesta], [RespuestaFalsa1], [RespuestaFalsa2], [RespuestaFalsa3]) VALUES (60, 6, 3, N'Habilidad Blanda: Responsabilidad Pregunta 9', N'Respuesta correcta.', N'Opción incorrecta 1', N'Opción incorrecta 2', N'Opción incorrecta 3')
INSERT [dbo].[Preguntas] ([id], [idHabilidad], [Dificultad], [Pregunta], [Respuesta], [RespuestaFalsa1], [RespuestaFalsa2], [RespuestaFalsa3]) VALUES (61, 6, 3, N'Habilidad Blanda: Responsabilidad Pregunta 10', N'Respuesta correcta.', N'Opción incorrecta 1', N'Opción incorrecta 2', N'Opción incorrecta 3')
INSERT [dbo].[Preguntas] ([id], [idHabilidad], [Dificultad], [Pregunta], [Respuesta], [RespuestaFalsa1], [RespuestaFalsa2], [RespuestaFalsa3]) VALUES (62, 1, 2, N'¿Por qué es importante adaptar tu comunicación al público objetivo?', N'Por los diferentes estilos de comunicación para comprender el mensaje. ', N'Todas las personas comprenden el mismo estilo de comunicaón.', N'Los mensajes son siempre entendidos de la misma manera ', N'La comunicación no necesita adaptarse.')
SET IDENTITY_INSERT [dbo].[Preguntas] OFF
GO
SET IDENTITY_INSERT [dbo].[Reglas] ON 

INSERT [dbo].[Reglas] ([id], [NombrePerfil], [ProbabilidadMenor], [ProbabilidadIgual], [ProbabilidadMayor], [PreguntasDificultad1], [MultiplicadorPreguntas]) VALUES (1, N'Predeterminado', 30, 60, 10, 5, 1.5)
SET IDENTITY_INSERT [dbo].[Reglas] OFF
GO
SET IDENTITY_INSERT [dbo].[Users] ON 

INSERT [dbo].[Users] ([id], [UserName], [FirstName], [LastName], [Department], [Position], [Password], [HashedPassword], [idGrupo], [Code], [AppInfo]) VALUES (1, N'arturo.navarrete', N'Arturo', N'Navarrete Velasco', N'Outsourcing', N'Enfermero', N'arturo123', 0x367538DF5E914DC4171368FE1E39A93E0A6E19A2198116114CC36F201EB7F64A000000000000000000000000000000000000, 1, N'', N'')
INSERT [dbo].[Users] ([id], [UserName], [FirstName], [LastName], [Department], [Position], [Password], [HashedPassword], [idGrupo], [Code], [AppInfo]) VALUES (2, N'kamal.saade', N'Kamal', N'Saade', N'Outsourcing', N'Administrador de software', N'kamal123', 0xCBE238B2E1ECBC7204A33AD8B425A931BDDDCEB4B4CCB08FB01B223A8E6E79DE000000000000000000000000000000000000, 2, N'', N'')
INSERT [dbo].[Users] ([id], [UserName], [FirstName], [LastName], [Department], [Position], [Password], [HashedPassword], [idGrupo], [Code], [AppInfo]) VALUES (3, N'stephanie.wellman', N'Stephanie', N'Wellman', N'Outsourcing', N'Administrador de software', N'stephanie123', 0x5FFBFBE3AD54BE84290B66D5F2C31D83194A2AB6A34F612C61AD75671FC9F187000000000000000000000000000000000000, 2, N'', N'')
INSERT [dbo].[Users] ([id], [UserName], [FirstName], [LastName], [Department], [Position], [Password], [HashedPassword], [idGrupo], [Code], [AppInfo]) VALUES (4, N'ayudante.prueba', N'Test', N'Coach 1', N'Enfermería', N'Jefe de enfermería', N'ayudante123', 0xD58CBBCD32734B746A9B82B2DD495C19198EBF5E45CCA4B659E5D2B8C2B46CC2000000000000000000000000000000000000, 3, N'', N'')
INSERT [dbo].[Users] ([id], [UserName], [FirstName], [LastName], [Department], [Position], [Password], [HashedPassword], [idGrupo], [Code], [AppInfo]) VALUES (5, N'enfermero.prueba', N'Test01', N'Enfermero', N'Enfermería', N'Enfermero', N'enfermero123', 0x19FD305B0CB9F93EADC499EEE28D1C0111FAE5BC955EFA1C05D543FD9791A271000000000000000000000000000000000000, 4, N'', N'')
INSERT [dbo].[Users] ([id], [UserName], [FirstName], [LastName], [Department], [Position], [Password], [HashedPassword], [idGrupo], [Code], [AppInfo]) VALUES (6, N'enfermera.prueba', N'Test02', N'Enfermera', N'Enfermería', N'Enfermera', N'enfermera123', 0xEB930D873394C2235D7C2E0F3017D356BD47567BD9B1EF50A848934DCE7A1D10000000000000000000000000000000000000, 4, N'', N'')
INSERT [dbo].[Users] ([id], [UserName], [FirstName], [LastName], [Department], [Position], [Password], [HashedPassword], [idGrupo], [Code], [AppInfo]) VALUES (7, N'angelica.alcanta', N'Angélica', N'Alcanta', N'Enfermería', N'Enfermera', N'angelica123', 0x0763AEB0B7A7403625ADEAF9C335C269746F2E5AA1A173C787016ABCAF6A5FEB000000000000000000000000000000000000, 4, N'', N'')
SET IDENTITY_INSERT [dbo].[Users] OFF
GO
ALTER TABLE [dbo].[Permissions]  WITH CHECK ADD CHECK  (([PermissionValue]>=(0) AND [PermissionValue]<=(63)))
GO
ALTER TABLE [dbo].[Permissions]  WITH CHECK ADD CHECK  (([PropertyName]='Visible' OR [PropertyName]='Enabled'))
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "RegistroRespuestas"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 237
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "RegistroRondas"
            Begin Extent = 
               Top = 5
               Left = 297
               Bottom = 135
               Right = 487
            End
            DisplayFlags = 280
            TopColumn = 2
         End
         Begin Table = "Habilidades"
            Begin Extent = 
               Top = 4
               Left = 515
               Bottom = 134
               Right = 714
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Niveles"
            Begin Extent = 
               Top = 141
               Left = 520
               Bottom = 271
               Right = 721
            End
            DisplayFlags = 280
            TopColumn = 6
         End
         Begin Table = "Users"
            Begin Extent = 
               Top = 6
               Left = 752
               Bottom = 136
               Right = 931
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 4755
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'View_RegistroRespuestas'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'View_RegistroRespuestas'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'View_RegistroRespuestas'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "RegistroRondas"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 228
            End
            DisplayFlags = 280
            TopColumn = 1
         End
         Begin Table = "Habilidades"
            Begin Extent = 
               Top = 6
               Left = 713
               Bottom = 136
               Right = 912
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Niveles"
            Begin Extent = 
               Top = 6
               Left = 474
               Bottom = 136
               Right = 675
            End
            DisplayFlags = 280
            TopColumn = 6
         End
         Begin Table = "Users"
            Begin Extent = 
               Top = 6
               Left = 266
               Bottom = 136
               Right = 445
            End
            DisplayFlags = 280
            TopColumn = 7
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'View_RegistroRondas'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'View_RegistroRondas'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Users"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 263
               Right = 217
            End
            DisplayFlags = 280
            TopColumn = 7
         End
         Begin Table = "Groups"
            Begin Extent = 
               Top = 6
               Left = 255
               Bottom = 185
               Right = 425
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'ViewUsers'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'ViewUsers'
GO
USE [master]
GO
ALTER DATABASE [NurseSupDB] SET  READ_WRITE 
GO
